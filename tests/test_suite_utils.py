#!/bin/python3

import os
from test_suite_funcs import *

def print_test_category_list(working_dir):
  print("Test category list:")
  for main_dirname, directorys, filenames in os.walk(working_dir + "tests/test-files/"):
    directorys.sort()
    for test_dir_name in directorys:
      print("  " + test_dir_name[2:])

def print_help():
  print("OPTIONS:")
  print("  -l, --list      Display the list of test categories.")
  print("  -c, --category  Execute the test suite on the categories passed in argument only.")
  print("  -s, --sanity    Execute the test suite with sanity checks enabled.")
  print("  -b, --binary    Specify de path to the binary (for manual mode).")
  print("  -h, --help      Display this screen.")

def norm_path(path):
  if path[-1] != '/':
    path = path + '/'
  return path

def execute_category_test(bin_path, flag_sani, main_dirname, dirname):
  for main_dirname, directorys, filenames in os.walk(main_dirname):
    for dir_name in directorys:
      if dirname == dir_name[2:]:
        make_dir_tests(bin_path, flag_sani, main_dirname, dir_name)
        return


#!/bin/python3

import json
import os
from subprocess import Popen, PIPE, TimeoutExpired

class Test(object):
  def __init__(self, name, description, cmd_line, type_cmd, timeout):
    self.name = name
    self.description = description
    self.cmd_line = cmd_line
    self.type_cmd = type_cmd
    self.timeout = timeout

def object_decoder(obj):
  if '__type__' in obj and obj['__type__'] == 'Test':
    return Test(obj['name'], obj['description'], obj['cmd_line'], obj['type'], obj['timeout'])
  return obj

def show_status(str1, str2):
  if str1 != str2:
    print(" \x1b[31m[KO]\x1b[0m ", end='')
    return 1
  else:
    print(" \x1b[32m[OK]\x1b[0m ", end='')
    return 0

def show_diff(log_file, test_line, expected, result, err_expected, err_res):
  log_file.write("Error in test : " + test_line + "\n")
  log_file.write("   expected out: " + expected[1:] + "\n")
  log_file.write("   expected err: " + err_expected[1:] + "\n")
  log_file.write("   result out: " + result[1:] + "\n")
  log_file.write("   result err: " + err_res[1:] + "\n")
  log_file.write("\n")

def get_decoded_testfile(file_name):
  test_file = open(file_name, 'r')
  test_parsed = json.load(test_file)
  return object_decoder(test_parsed)

def pretty_str_c_cmd(test_decoded):
  if len(test_decoded['cmd_line']) > 33:
    return '%-18s  %-45s' % (test_decoded['name'][0:18], "42sh -c \'" + test_decoded['cmd_line'][0:30] + "...")
  else:
    return '%-18s  %-45s' % (test_decoded['name'][0:18], "42sh -c \'" + test_decoded['cmd_line'][0:33] + "\'")

def pretty_str_no_arg_cmd(test_decoded):
  if len(test_decoded['cmd_line']) > 33:
    return '%-18s  %-45s' % (test_decoded['name'][0:18], "42sh \'" + test_decoded['cmd_line'][0:30] + "...")
  else:
    return '%-18s  %-45s' % (test_decoded['name'][0:18], "42sh \'" + test_decoded['cmd_line'][0:33] + "\'")

def make_c_42sh_cmd_line(bin_path, cmd_line):
  return [bin_path + "42sh"] + ['-c'] + [cmd_line]

def make_no_arg_42sh_cmd_line(bin_path, project_path, cmd_line):
  return [bin_path + "42sh"] + [project_path + "tests/scripts/" + cmd_line]

def make_c_bash_cmd_line(cmd_line):
  return  ['bash'] + ['--posix'] + ['-c'] + [cmd_line]

def make_no_arg_bash_cmd_line(project_path, cmd_line):
  return  ['bash'] + ['--posix'] + [project_path + "tests/scripts/" + cmd_line]

def exec_cmd_line(cmd_line, time_out):
  prosses = Popen(cmd_line, stdout=PIPE, stderr=PIPE)
  (output, err) = ("", "")
  flag_timeout = 0
  try:
    (output, err) = prosses.communicate(timeout=time_out)
  except TimeoutExpired:
    prosses.kill()
    (output, err) = prosses.communicate()
    flag_timeout = 1

  exit_code = prosses.wait()
  return (output, err, exit_code, flag_timeout)

def sanity_check(cmd_line, time_out):
  line = ['valgrind'] + cmd_line
  prosses = Popen(line, stdout=PIPE, stderr=PIPE)
  (output, err) = ("", "")
  try:
    (output, err) = prosses.communicate(timeout=time_out)
  except TimeoutExpired:
    prosses.kill()
    (output, err) = prosses.communicate()

  exit_code = prosses.wait()
  err = str(err)
  return err.find("no leaks are possible")

def make_test(bin_path, project_path, flag_sani, file_name, log_file):
  test_decoded = get_decoded_testfile(file_name)

  if test_decoded['type'] == '-c':
    test_line = pretty_str_c_cmd(test_decoded)
    cmd_42sh = make_c_42sh_cmd_line(bin_path, test_decoded['cmd_line'])
    cmd_bash = make_c_bash_cmd_line(test_decoded['cmd_line'])
  elif test_decoded['type'] == 'no_arg':
    test_line = pretty_str_no_arg_cmd(test_decoded)
    cmd_42sh = make_no_arg_42sh_cmd_line(bin_path, project_path, test_decoded['cmd_line'])
    cmd_bash = make_no_arg_bash_cmd_line(project_path, test_decoded['cmd_line'])

  print(" " + test_line, end='')
  (output_42sh, err_42sh, exit_code_42sh, timeout_42sh) = exec_cmd_line(cmd_42sh, int(test_decoded['timeout']))
  (output_bash, err_bash, exit_code_bash, timeout_bash) = exec_cmd_line(cmd_bash, int(test_decoded['timeout']))
  if timeout_42sh == 1 or timeout_bash == 1:
    print("timeout...")
    res = 1
  else:
    print("out", end='')
    res = show_status(str(output_bash), str(output_42sh))
    print("err", end='')
    res += show_status(str(err_bash), str(err_42sh))
    print("exit", end='')
    res += show_status(str(exit_code_42sh), str(exit_code_42sh))
    if flag_sani == 1:
      print("sanity", end='')
      if sanity_check(cmd_42sh, int(test_decoded['timeout'])) == -1:
        res += show_status("a", "b")
      else:
        res += show_status("a", "a")
    print("")
  if res != 0:
    show_diff(log_file, str(test_decoded['name']) + "\n   Description: " + str(test_decoded['description']) + "\n   Type: " + str(test_decoded['type']) + "\n   Test value: '" + str(test_decoded['cmd_line']) + "'", str(output_bash), str(output_42sh), str(err_bash), str(err_42sh))
  return res

def print_stats(nb_tests, succes_count):
  if nb_tests == 0 and succes_count == 0:
    return
  print("----- Succes rate : " + str(succes_count) + "/" + str(nb_tests), end='')
  print(" (%.0f%%) -----" % ((succes_count / nb_tests) * 100))

def make_dir_tests(bin_path, project_path, flag_sani, main_dir, test_dir_name, log_file):
  succes_count = 0
  nb_tests = 0
  for dirname, _, filenames in os.walk(main_dir + test_dir_name):
    filenames.sort()
    if len(filenames) == 0:
      return (0, 0)
    print("*** " + test_dir_name[2:] + " tests ***")
    print("")
    for filename in filenames:
      file_name = os.path.join(dirname, filename)
      res = make_test(bin_path, project_path, flag_sani, file_name, log_file)
      if res == 0:
        succes_count += 1
      nb_tests += 1
  print("")
  print(" ", end = '')
  print_stats(nb_tests, succes_count)
  return (nb_tests, succes_count)


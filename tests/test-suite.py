#!/bin/python3

import sys
import json
import os
from subprocess import Popen, PIPE
from test_suite_funcs import *
from test_suite_utils import *

flag_list = 0
flag_cate = 0
category_name = ""
flag_sani = 0
flag_bina = 0
bin_path = "./"
flag_help = 0
arg_dir = ""

if len(sys.argv) == 1:
  print("Usage: [options] [project_dir]")
  print(" -h or --help to list options")
  exit()

arg = 1
while(arg < len(sys.argv)):
  if (sys.argv[arg] in ["-l", "--list"]):
    flag_list = 1
    arg += 1
  elif (sys.argv[arg] in ["-c", "--category"]):
    flag_cate = 1
    category_name = sys.argv[arg + 1]
    arg += 2
  elif (sys.argv[arg] in ["-s", "--sanity"]):
    flag_sani = 1
    arg += 1
  elif (sys.argv[arg] in ["-b", "--binary"]):
    flag_bina = 1
    bin_path =  os.path.abspath(sys.argv[arg + 1])
    arg += 2
  elif (sys.argv[arg] in ["-h", "--help"]):
    flag_help = 1
    arg += 1
  else:
    arg_dir = str(sys.argv[arg])
    arg += 1

arg_dir = norm_path(os.path.abspath(arg_dir))
bin_path = norm_path(bin_path)

if flag_help == 1:
  print_help()
  exit()

nb_tests = 0
succes_test = 0

if not os.path.exists(arg_dir + "tests/test-files/"):
  print("Invalide project dir path !")
  exit()

if flag_list == 1:
  print_test_category_list(arg_dir)
  exit()

if not os.path.exists(bin_path + "42sh"):
  print("Invalide bin path !")
  exit()

log_file = open("test-suite.log", "w")

print("***********************")
print("*** 42sh test-suite ***")
print("***********************")
print("")

if flag_cate == 1:
  execute_category_test(bin_path, flag_sani, arg_dir + "tests/test-files/", category_name)
  exit()

for main_dirname, directorys, filenames in os.walk(arg_dir + "tests/test-files/"):
  directorys.sort()

  for dirname in directorys:
    (tmp_nb_tests, tmp_succes_tests) = make_dir_tests(bin_path, arg_dir, flag_sani, main_dirname, dirname, log_file)
    nb_tests += tmp_nb_tests
    succes_test += tmp_succes_tests
    print("")

print_stats(nb_tests, succes_test)

log_file.close()

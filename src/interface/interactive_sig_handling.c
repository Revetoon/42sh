#include <signal.h>
#include <stdio.h>
#include <readline/readline.h>

#include "interactive_sig_handling.h"

void my_sig_hand(int sig)
{
  rl_echo_signal_char(sig);
  signal(sig, my_sig_hand);
  if (!rl_outstream)
    fflush(stdout);
  else
    fflush(rl_outstream);
}

void set_interactive_signal_handling(void)
{
  rl_catch_signals = 0;
  rl_set_signals();
  signal(SIGINT, my_sig_hand);
  signal(SIGHUP, my_sig_hand);
  signal(SIGQUIT, my_sig_hand);
  signal(SIGTSTP, my_sig_hand);
  signal(SIGTTIN, my_sig_hand);
  signal(SIGTTOU, my_sig_hand);
}

void unset_interactive_signal_handling(void)
{
  rl_clear_signals();
  signal(SIGINT, SIG_DFL);
  signal(SIGHUP, SIG_DFL);
  signal(SIGQUIT, SIG_DFL);
  signal(SIGTSTP, SIG_DFL);
  signal(SIGTTIN, SIG_DFL);
  signal(SIGTTOU, SIG_DFL);
}
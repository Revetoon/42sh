#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "buffer.h"
#include "interactive_sig_handling.h"
#include "interactive.h"

void print_prompt(int ps)
{
  if (ps == 1)
    fprintf(stderr, "42sh$ ");
  else
    fprintf(stderr, "> ");
  fflush (stderr);
}

char *interactive_mode(int ps)
{
  static int set_history = 0;
  if (!set_history)
  {
    using_history(); /* Init the history functionalitys */
    set_history = 1;
  }

  set_interactive_signal_handling();
  print_prompt(ps);
  rl_already_prompted = 1;
  char *line = readline("42sh$ ");

  unset_interactive_signal_handling();

  if (!line || *line == '\n')
    return line;

  add_history(line);

  size_t size_line = strlen(line);
  char *save = realloc(line, size_line + 2);
  if (!save)
  {
    free(line);
    perror("42sh: cannot do realloc()");
    exit(2);
  }
  save[size_line] = '\n';
  save[size_line + 1] = '\0';
  return save;
}

char *read_line_file(int fd_in)
{
  struct buffer buffer = { .size = 0, .buffer = NULL };
  char c = 0;
  ssize_t r = read(fd_in, &c, 1);
  while (r && c != '\n')
  {
    add_buffer(&buffer, &c, 1);
    r = read(fd_in, &c, 1);
  }
  if (r)
    add_buffer(&buffer, &c, 1);
  if (buffer.buffer && *buffer.buffer != '\0')
    return buffer.buffer;
  free(buffer.buffer);
  return NULL;
}

char *get_next_line(int fd_in, int ps)
{
  static int fd = -1;
  if (fd == -1 || fd_in != -1)
    fd = fd_in;
  if (fd == -1)
    return NULL;

  if (isatty(fd))
    return interactive_mode(ps);
  else
    return read_line_file(fd);
}

char *append_next_line(char *old)
{
  char *new = get_next_line(-1, 2);
  if (!new)
    return NULL;
  size_t size_new = strlen(new);
  size_t size_old = strlen(old);
  char *concat = calloc((size_old + size_new + 1), sizeof(char));
  if (!concat)
  {
    perror("42sh: cannot do malloc()");
    exit(2);
  }
  strncpy(concat, old, size_old);
  strncpy(concat + size_old, new, size_new);
  free(old);
  free(new);
  return concat;
}
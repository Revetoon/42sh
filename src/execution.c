#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "interactive.h"
#include "exec_ast.h"
#include "parser_free.h"
#include "parser.h"
#include "ast_print.h"

#include "execution.h"

int forty_two_sh(struct environment *env, int fd_in)
{
  char *line = NULL;
  while(1)
  {
    line = get_next_line(fd_in, 1);
    if (!line)
      break;

    struct ast_node *ast = parser(line, env);
    if (env->print_ast)
      ast_print(ast);
    struct command_args args;
    args.env = env;
    bool return_value = exec_asts(ast, &args);
    free_ast(ast);

    if (return_value)
      break;
  }
  return env->previous_return;
}

int execut_file(const char *file_name, struct environment *env)
{
  int fd = open(file_name, O_RDONLY | O_CLOEXEC);
  if (fd == -1)
    return -1;

  int res = forty_two_sh(env, fd);
  close(fd);
  return res;
}
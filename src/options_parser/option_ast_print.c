/**
** @file option_ast_print.c
** @brief Option ast print
** @authors xu_f
*/

# include "options_parser_include.h"

bool option_ast_print(struct option_parsing_args *args)
{
  args->env->print_ast = true;
  args->env->previous_return = 0;
  return false;
}
/**
** @file option_version.c
** @brief Option version
** @authors xu_f
*/

# include "options_parser_include.h"

void print_version(FILE *output)
{
  fprintf(output, "Version %1.1f\n", PROJECT_VERSION);
}

bool option_version(struct option_parsing_args *args)
{
  print_version(stdout);
  args->env->previous_return = 0;
  return true;
}
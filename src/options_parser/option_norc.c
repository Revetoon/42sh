/**
** @file option_norc.c
** @brief Option norc
** @authors xu_f
*/

# include "options_parser_include.h"

bool option_norc(struct option_parsing_args *args)
{
  args->env->norc = true;
  return false;
}
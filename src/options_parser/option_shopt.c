/**
** @file option_shopt.c
** @brief Option shopt
** @authors xu_f
*/

# include "options_parser_include.h"

static bool option_shopt(struct option_parsing_args *args, bool plus)
{
  int arg_index = *args->arg_index + 1;
  if (arg_index == args->argc)
    return false;
  (void)plus;
  ++*args->arg_index;
  return false;
}

bool option_shopt_plus(struct option_parsing_args *args)
{
  return option_shopt(args, true);
}

bool option_shopt_minus(struct option_parsing_args *args)
{
  return option_shopt(args, false);
}
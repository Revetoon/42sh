/**
** @file option_command.c
** @brief Option command -c
** @authors xu_f
*/
# include <unistd.h>
# include <sys/types.h>
# include <errno.h>
# include <wait.h>
# include <stdlib.h>
#include <environment.h>
#include <command_args.h>
#include <parser.h>
#include <memory.h>
#include <exec_ast.h>
# include "options_parser_include.h"
# include "parser_free.h"

bool option_command(struct option_parsing_args *args)
{
  int arg_index = *args->arg_index + 1;
  if (arg_index == args->argc)
  {
    fputs("42sh: -c: option requires an argument\n", stderr);
    args->env->previous_return = 2;
    return true;
  }
  struct command_args arg;
  arg.env = args->env;
  size_t len = strlen(args->argv[arg_index]);
  char *execute = calloc(len + 2, sizeof (char));
  strcpy(execute, args->argv[arg_index]);
  execute[len] = '\n';
  struct ast_node *ast = parser(execute, args->env);
  exec_asts(ast, &arg);
  free_ast(ast);
  return true;
}
/**
** @file options_parser.c
** @brief Option parser wrapper
** @authors xu_f
*/
# include <stdlib.h>
# include <string.h>
# include <stdbool.h>
# include <stdio.h>
# include "options_parser.h"
# include "options_parser_include.h"

#define PRINT_OFFSET 8

/**
* @struct option
* @brief Option parser wrapper structure
*/
struct option
{
  char *name;
  /*!< name of the option */
  bool (*func)(struct option_parsing_args*);
  /*!< function to parse the option */
};

static struct option short_options[] = {
  {"--", NULL},
  {"-c", option_command},
  {"+O", option_shopt_plus},
  {"-O", option_shopt_minus},
  {NULL, NULL}
};

static struct option long_options[] = {
  {"--version", option_version},
  {"--norc", option_norc},
  {"--ast-print", option_ast_print},
  {NULL, NULL}
};

void print_usage(FILE *output)
{
  fputs("Usage: 42sh [GNU long options] [options] [file]\n", output);
  fputs("GNU long options:\n", output);
  for (struct option *opt = long_options; opt->name; ++opt)
    fprintf(output, "%*s%s\n", PRINT_OFFSET, "", opt->name);
  fputs("options:\n", output);
  for (struct option *opt = short_options; opt->name; ++opt)
    fprintf(output, "%*s%s\n", PRINT_OFFSET, "", opt->name);
  fflush(output);
}

static bool option_not_found(int argc, const char **argv, const int *arg_index,
                             struct environment *env)
{
  if (*arg_index >= argc)
  {
    env->previous_return = 0;
    return false;
  }
  fprintf(stderr, "42sh: %s: invalid option\n", argv[*arg_index]);
  print_usage(stderr);
  env->previous_return = 2;
  return true;
}

static int my_substr(const char *option, const char *string)
{
  for (; *option && *string; ++option, ++string)
  {
    if (*option != *string)
      return 0;
  }
  if (*string && !*option)
    return 0;
  if (*option)
    return 1;
  return 2;
}

static const struct option *find_option(const struct option *options,
                                        const char *string, bool long_option)
{
  bool find_substring = long_option;
  if (my_substr(string, long_option ? "--" : "-") != 1)
    return NULL;
  bool found_substring = false;
  const struct option *found_option = NULL;
  while (options->name)
  {
    int return_value = my_substr(options->name, string);
    if (return_value == 2)
      return options;
    if (find_substring && return_value == 1)
    {
      if (found_substring)
        find_substring = false;
      else
      {
        found_option = options;
        found_substring = true;
      }
    }
    ++options;
  }
  return find_substring ? found_option : (struct option*)-1;
}

bool parse_bash_options(int argc, const char **argv,
                       int *arg_index, struct environment *env)
{
  const struct option *option;
  static struct option *opts[] = {long_options, short_options};
  for (size_t i = 0; i < 2; ++i)
  {
    while (*arg_index < argc
           && (option = find_option(opts[i], argv[*arg_index], i == 0)))
    {
      if (option == (struct option *) -1)
        return option_not_found(argc, argv, arg_index, env);
      if (!option->func) //parsing long option that stop option parsing
        return -1;
      struct option_parsing_args args = {
        .argc = argc,
        .argv = argv,
        .arg_index = arg_index,
        .env = env
      };
      if (option->func(&args))
        return true;
      ++*arg_index;
    }
  }
  return false;
}
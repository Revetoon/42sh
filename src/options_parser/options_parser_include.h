/**
** @file options_parser_include.h
** @brief Option parser wrapper header
** @authors xu_f
*/

#ifndef INC_42SH_OPTIONS_PARSER_INCLUDE_H
#define INC_42SH_OPTIONS_PARSER_INCLUDE_H

# include <stdio.h>
# include "environment.h"
# include "main_42sh.h"

/**
** @struct option_parsing_args
** @brief Option parser arguments
*/
struct option_parsing_args
{
  int argc;
  /*!< number of argument in argv */
  const char **argv;
  /*!< array of argument */
  int *arg_index;
  /*!< current index in argv */
  struct environment *env;
  /*!< environment */
};

/**
** @brief Print usage function
** @param output output FILE
*/
void print_usage(FILE *output);

/**
** @brief Option command -c parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_command(struct option_parsing_args *args);

/**
** @brief Print version
** @param output output FILE to print version
*/
void print_version(FILE *output);

/**
** @brief Option version parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_version(struct option_parsing_args *args);

/**
** @brief Option shopt O+ parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_shopt_plus(struct option_parsing_args *args);

/**
** @brief Option shopt O- parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_shopt_minus(struct option_parsing_args *args);

/**
** @brief Option norc parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_norc(struct option_parsing_args *args);

/**
** @brief Option ast-print parser
** @param args option parser arguments
** @return true if 42sh must exit
*/
bool option_ast_print(struct option_parsing_args *args);

#endif /* !INC_42SH_OPTIONS_PARSER_INCLUDE_H */
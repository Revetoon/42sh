/**
** @file environment.c
** @brief Environment variable structure functions
** @authors xu_f
*/

# include <stdio.h>
# include <string.h>
# include <assert.h>
# include "env_var.h"

struct env_var *env_var_init(const char *key, const char *value,
                             bool exported, bool readonly)
{
  struct env_var *var = calloc(1, sizeof (struct env_var));
  if (!var)
  {
    fputs("Memory exhausted\n", stderr);
    exit(2);
  }
  var->key = strdup(key);
  var->value = strdup(value);
  if (!var->key || !var->value)
  {
    fprintf(stderr, "Memory exhausted when duplicating string %s\n", key);
    exit(2);
  }
  var->exported = exported;
  var->readonly = readonly;
  return var;
}

int env_var_set(struct env_var *env_var, const char *new_value)
{
  assert(env_var);
  if (env_var->readonly)
    return 1;
  free(env_var->value);
  env_var->value = NULL;
  env_var->value = strdup(new_value);
  if (!env_var->value)
  {
    fprintf(stderr, "Memory exhausted when duplicating string %s\n",
            env_var->key);
    exit(2);
  }
  return 0;
}

struct env_var *env_var_dup(struct env_var *var)
{
  assert(var);
  return env_var_init(var->key, var->value, var->exported, var->readonly);
}

void env_var_free(struct env_var *var)
{
  assert(var);
  free(var->value);
  free(var->key);
  var->value = NULL;
  var->key = NULL;
  free(var);
}
/**
** @file env_func.h
** @brief Environment function structure header
** @authors xu_f
*/

#ifndef INC_42SH_ENV_FUNC_H
#define INC_42SH_ENV_FUNC_H

/**
** @struct env_func
** @brief en_func structure
*/
struct env_func
{
  char *name; /*!< name of the function */
  struct ast_node *function; /*!< commands ast of the function */
};

/**
** @brief Initialize structure env_func
** @param ast ast_node of type function
** @return Allocated and initialized env_func
**
** The command list is detached from the ast node and replaced by NULL
*/
struct env_func *env_func_init(struct ast_node *ast);

#endif /* !INC_42SH_ENV_FUNC_H */
/**
** @file environment.c
** @brief Environment functions
** @authors xu_f
*/

#include <stdlib.h>
#include <stdio.h>
# include <unistd.h>
#include <memory.h>
# include "hash_table.h"
# include "environment.h"
# include "env_var.h"

struct environment *init_environment(void)
{
  struct environment *env = calloc(1, sizeof (struct environment));
  if (!env)
  {
    fprintf(stderr, "Memory allocation failed\n");
    return NULL;
  }
  env->env_vars = create_htable(10);
  env->env_func = create_htable(10);
  env->argc = 1;
  env->argv = calloc(2, sizeof (char*));
  env->argv[0] = "42sh";
  init_export_env(env);
  return env;
}

void set_var_environment(struct environment *env, char *key, char *value,
                         bool exported, bool readonly)
{
  struct env_var *var = NULL;
  var = get_env_var(env, key);
  if (!var)
  {
    var = env_var_init(key, value, exported, readonly);
    add_htable(env->env_vars, var->key, var);
    if (var->exported && setenv(var->key, var->value, 1) < 0)
      fprintf(stderr, "Variable export failed\n");
  }
  else
  {
    if (var->readonly && value)
    {
      fprintf(stderr, "%s readonly variable\n", key);
      env->previous_return = 1;
      return;
    }
    if (var->exported && setenv(var->key, value, 1) < 0)
      fprintf(stderr, "Variable export failed\n");
    if (exported)
      var->exported = exported;
    free(var->value);
    var->value = strdup(value);
    if (readonly)
      var->readonly = readonly;
  }
}

char *get_var_environment(struct environment *env, char *key)
{
  struct env_var *var = get_env_var(env, key);
  if (var)
  {
    if (var->exported)
    {
      free(var->value);
      var->value = strdup(getenv(key));
    }
    return strdup(var->value);
  }
  else
  {
    char *value = getenv(key);
    if (value)
    {
      set_var_environment(env, key, value, true, false);
      return strdup(value);
    }
    return strdup("");
  }
}

struct environment *export_environment(struct environment *env)
{
  struct environment *export_env = init_environment();
  size_t nb_vars = 0;
  void **env_vars = get_all_value_htable(env->env_vars, &nb_vars);
  for (size_t i = 0; i < nb_vars; ++i)
  {
    struct env_var *var = env_vars[i];
    if (var->exported)
    {
      set_var_environment(export_env, var->key, var->value,
                          var->exported, var->readonly);
    }
  }
  free(env_vars);
  return export_env;
}

bool env_var_isset(struct environment *env, char *key)
{
  struct env_var *var = get_env_var(env, key);
  if (var)
    return true;
  else
  {
    char *value = getenv(key);
    if (value)
    {
      set_var_environment(env, key, value, true, false);
      return true;
    }
    return false;
  }
}
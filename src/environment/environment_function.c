/**
** @file environment_function.c
** @brief Environment function functions
** @authors xu_f
*/

#include <hash_table.h>
#include <parser_free.h>
# include "environment.h"
# include "env_func.h"

void set_func_environment(struct environment *env, struct ast_node *ast)
{
  struct pair *pair = access_htable(env->env_func,
                                    ast->value.function_value->name);
  struct function_node *fun = ast->value.function_value;
  if (pair)
  {
    struct env_func *func = pair->value;
    free_ast(func->function);
    func->function = fun->command_list;
    fun->command_list = NULL;
    return;
  }
  struct env_func *func = env_func_init(ast);
  if (!func)
    return;
  add_htable(env->env_func, func->name, func);
}

struct ast_node *get_func_environment(struct environment *env, char *key)
{
  struct pair *pair = access_htable(env->env_func, key);
  if (!pair)
    return NULL;
  struct env_func *func = pair->value;
  return func->function;
}
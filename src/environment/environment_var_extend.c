/**
** @file environment_var_extend.c
** @brief Environment variable function extended
** @authors xu_f
*/
# include <unistd.h>
# include <string.h>
# include "env_var.h"

struct env_var *get_env_var(struct environment *env, char *key)
{
  if (!env)
    return NULL;
  struct pair *return_value = access_htable(env->env_vars, key);
  if (return_value)
    return return_value->value;
  return NULL;
}

void set_exported(struct environment *env, char *var)
{
  struct env_var *env_var = get_env_var(env, var);
  if (!env_var)
  {
    set_var_environment(env, var, "", true, false);
    return;
  }
  env_var->exported = true;
  setenv(env_var->key, env_var->value, 1);
}

void remove_exported(struct environment *env, char *var)
{
  struct env_var *env_var = get_env_var(env, var);
  if (env_var)
  {
    unsetenv(env_var->key);
    env_var->exported = false;
  }
}

void init_export_env(struct environment *env)
{
  for (char **c = __environ; *c; ++c)
  {
    char *dup = strdup(*c);
    char *value = NULL;
    dup = strtok_r(dup, "=", &value);
    set_var_environment(env, dup, value, true, false);
    free(dup);
  }
}

void modify_var_environment(struct environment *env, char *key, char *value)
{
  struct env_var *var = NULL;
  var = get_env_var(env, key);
  if (var)
    set_var_environment(env, key, value, var->exported, var->readonly);
  else
    set_var_environment(env, key, value, false, false);
}
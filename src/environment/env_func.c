/**
** @file env_func.c
** @brief Environment function structure functions
** @authors xu_f
*/

# include <stdlib.h>

# include <string.h>
#include <assert.h>
#include <ast.h>
#include <stdio.h>
# include "environment.h"
# include "env_func.h"

struct env_func *env_func_init(struct ast_node *ast)
{
  assert(ast);
  assert(ast->type == NODE_FUNCTION);
  struct function_node *node = ast->value.function_value;
  struct env_func *env = malloc(sizeof(struct env_func));
  env->name = strdup(node->name);
  if (!env->name)
  {
    free(env);
    fprintf(stderr, "function name cannot be null");
    return NULL;
  }
  env->function = node->command_list;
  node->command_list = NULL;
  return env;
}
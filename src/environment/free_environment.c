/**
** @file free_environment.c
** @brief Environment free functions
** @authors xu_f
*/
# include <stdlib.h>
#include <parser_free.h>
# include "environment.h"
# include "hash_table.h"
#include "env_var.h"
#include "env_func.h"

static void free_env_vars(struct environment *env)
{
  size_t nb_vars = 0;
  void **env_vars = get_all_value_htable(env->env_vars, &nb_vars);
  for (size_t i = 0; i < nb_vars; ++i)
  {
    struct env_var *var = env_vars[i];
    free(var->key);
    free(var->value);
    free(var);
  }
  free_htable(env->env_vars);
  free(env_vars);
}

static void free_env_function(struct environment *env)
{
  size_t nb_vars = 0;
  void **functions = get_all_value_htable(env->env_func, &nb_vars);
  for (size_t i = 0; i < nb_vars; ++i)
  {
    struct env_func *func = functions[i];
    free_ast(func->function);
    free(func->name);
    free(func);
  }
  free_htable(env->env_func);
  free(functions);
}

void free_environment(struct environment *env)
{
  if (!env)
    return;
  free(env->argv);
  free_env_vars(env);
  free_env_function(env);
  free(env);
}
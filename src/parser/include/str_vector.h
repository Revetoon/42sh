/**
** @file str_vector.h
** @brief str vector
*/
# ifndef STR_VECTOR_H
# define STR_VECTOR_H

struct str_vector
{
    char *data;
    size_t capacity;
    size_t size;
    size_t index;
};

/**
 * doubles the capacity of the struct str_vector
 * @param v the str_vector
 */
void double_vect_size(struct str_vector *v);
/**
 * Initialize a struct str_vector
 * @param v the struct str_vector
 */
void init_str_vector(struct str_vector *v);

/**
 * frees the struct str_vector
 * @param v the struct str_vector
 */
void free_str_vector_data(struct str_vector *v);

/**
 * adds a string to the given struct str_vector
 * @param v   the struct str_vector
 * @param str the string to be added
 */
void str_vector_add(struct str_vector *v, const char *str);


# endif
/**
** @file parser_line_helper.h
** @brief parser line helper
*/
# ifndef PARSER_LINE_HELPER_H
# define PARSER_LINE_HELPER_H

# include "parser.h"

/**
 * initialize the current line struct
 * @param l the initialized line
 */
void init_line(struct parsable_line *l);

/**
 * returns the current line struct
 * @return the current line struct
 */
struct parsable_line *get_line_struct();

/**
 * frees the current line struct
 * @param l the line struct
 */
void free_line(struct parsable_line *l);

# endif
/**
** @file lexer.h
** @brief lexer
*/
# ifndef LEXER_H
# define LEXER_H

/**
 * Lexes the given string: transform a string into a list of tokens
 * @param  line the input string
 * @return      a structure containing the tokenized string
 */
struct parsable_line *lexer(char *line);

# endif
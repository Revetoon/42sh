/**
** @file ast_helper.h
** @brief ast helper
*/
# ifndef AST_HELPERS_H
# define AST_HELPERS_H

# include "ast.h"

/**
 * creates a new ast_node
 * @param  type the noe type
 * @return      the new node
 */
struct ast_node *new_node(enum node_type type);

/**
 * adds a redirection to the given ast_node
 * @param  node     the ast node
 * @param  redirect the redirection node
 * @return          the resulting node
 */
struct ast_node *add_redirection(struct ast_node *node, struct ast_node *redirect);

/**
 * returns the type of redirection that correspond to the given string
 * @param  redir the string representing a redirection
 * @return       the type of redirection
 */
enum redirection_type get_redir_type(char *redir);

# endif
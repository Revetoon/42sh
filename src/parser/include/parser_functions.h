/**
** @file parser_functions.h
** @brief parser functions
*/
# ifndef PARSER_FUNCTIONS_H
# define PARSER_FUNCTIONS_H

# include "parser.h"

/**
 * This function is part of the parsing.
 * Accepts a given token from the currently parsed line
 * @param  tk the token to accept
 * @return    1 if the token was accepted, 0 else.
 */
int accept(e_token tk);

/**
 * This function is part of the parsing.
 * Counts how many of the next tokens are GENERIC
 * @return the token count
 */
int count_args();

/**
 * This function is part of the parsing.
 * Counts how many of the next tokens are 'items'
 * @return the token count
 */
int count_items();

/**
 * This function is part of the parsing.
 * Accepts any number of backreturn tokens
 */
void accept_backreturns();


# endif
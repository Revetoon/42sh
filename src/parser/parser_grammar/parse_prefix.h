/**
** @file
*/
# ifndef PARSE_PREFIX_H
# define PARSE_PREFIX_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_prefix();

# endif

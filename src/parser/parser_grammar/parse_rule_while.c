/* parse_rule_while.c */

# include "parse_rule_while.h"
# include "parse_do_group.h"
# include "parse_compound_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

struct ast_node *parse_rule_while()
{
    if (!accept(TK_WHILE))
    {
        return NULL;
    }
    struct ast_node *n = parse_compound_list();
    if (!n)
        return NULL;
    struct ast_node *m = parse_do_group();
    if (!m)
    {
        free_ast(n);
        return NULL;
    }
    struct ast_node *node = new_node(NODE_WHILE);
    node->value.while_value->condition = n;
    node->value.while_value->do_group = m;
    return node;
}
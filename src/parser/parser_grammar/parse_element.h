/**
** @file
*/
# ifndef PARSE_ELEMENT_H
# define PARSE_ELEMENT_H

# include "ast.h"

/**
 * Parse an element
 * @return a string element
 */
char *parse_element();

# endif

/**
** @file
*/
# ifndef PARSE_SIMPLE_COMMAND_H
# define PARSE_SIMPLE_COMMAND_H
# include <stdbool.h>
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_simple_command(bool invert);

# endif

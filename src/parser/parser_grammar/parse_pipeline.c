/* parse_pipeline.c */

# include <stdbool.h>

# include "parse_pipeline.h"
# include "parse_command.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

static struct ast_node *parse_pipeline_pipe(struct ast_node *left)
{
    if (!accept(TK_PIPE))
        return NULL;
    accept_backreturns();
    struct ast_node *pipe = new_node(NODE_PIPE);
    pipe->value.pipe_value->left = left;
    struct ast_node *n = parse_command(false);
    if (!n)
    {
        free_ast(pipe);
        return NULL;
    }
    pipe->value.pipe_value->right = n;
    return pipe;
}

struct ast_node *parse_pipeline()
{
    struct parsable_line *line = get_line_struct();
    bool invert = false;
    if (accept(TK_EXCLA))
        invert = true;
    int save = line->index;
    struct ast_node *n = parse_command(invert);
    if (!n)
        return NULL;
    struct ast_node *node = n;
    while (1)
    {
        save = line->index;
        n = parse_pipeline_pipe(node);
        if (!n)
        {
            line->index = save;
            break;
        }
        node = n;
    }
    return node;
}
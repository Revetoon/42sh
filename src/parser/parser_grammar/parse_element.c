/* parse_element.c */

# include "parse_element.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

char *parse_element()
{
    struct parsable_line *line = get_line_struct();
    char *el = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    return el;
}
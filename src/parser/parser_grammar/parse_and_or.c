/* parse_and_or.c */

# include "parse_and_or.h"
# include "parse_pipeline.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

static struct ast_node *parse_and_or_and(struct ast_node *left)
{
    if (!accept(TK_AND))
        return NULL;
    accept_backreturns();
    struct ast_node *and_or = new_node(NODE_AND);
    struct ast_node *n = parse_pipeline();
    if (!n)
    {
        free_ast(and_or);
        return NULL;
    }
    and_or->value.and_value->left = left;
    and_or->value.and_value->right = n;
    return and_or;
}

static struct ast_node *parse_and_or_or(struct ast_node *left)
{
    if (!accept(TK_OR))
        return NULL;
    accept_backreturns();
    struct ast_node *and_or = new_node(NODE_OR);
    struct ast_node *n = parse_pipeline();
    if (!n)
    {
        free_ast(and_or);
        return NULL;
    }
    and_or->value.or_value->left = left;
    and_or->value.or_value->right = n;
    return and_or;
}

struct ast_node *parse_and_or()
{
    struct parsable_line *line = get_line_struct();
    struct ast_node *n = parse_pipeline();
    if (!n)
        return NULL;
    int save = line->index;
    struct ast_node *node = n;
    while (1)
    {
        save = line->index;
        n = parse_and_or_and(node);
        if (!n)
        {
            line->index = save;
            n = parse_and_or_or(node);
            if (!n)
                break;
        }
        node = n;
    }
    return node;

}
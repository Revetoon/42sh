/* parse_rule_until.c */

# include "parse_rule_until.h"
# include "parse_do_group.h"
# include "parse_compound_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

struct ast_node *parse_rule_until()
{
    if (!accept(TK_UNTIL))
    {
        return NULL;
    }
    struct ast_node *n = parse_compound_list();
    if (!n)
        return NULL;
    struct ast_node *m = parse_do_group();
    if (!m)
    {
        free_ast(n);
        return NULL;
    }
    struct ast_node *node = new_node(NODE_UNTIL);
    node->value.until_value->condition = n;
    node->value.until_value->do_group = m;
    return node;
}
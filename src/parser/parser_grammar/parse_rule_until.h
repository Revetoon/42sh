/**
** @file
*/
# ifndef PARSE_RULE_UNTIL_H
# define PARSE_RULE_UNTIL_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_rule_until();

# endif

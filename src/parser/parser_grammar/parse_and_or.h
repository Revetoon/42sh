/**
** @file
*/
# ifndef PARSE_AND_OR_H
# define PARSE_AND_OR_H

# include "ast.h"

/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_and_or();

# endif
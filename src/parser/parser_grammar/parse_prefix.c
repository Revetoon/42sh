/* parse_prefix.c */

# include <stdlib.h>
# include <string.h>

# include "parse_prefix.h"
# include "parse_redirection.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

static struct ast_node *parse_assignement_node()
{
    struct parsable_line *line = get_line_struct();
    char *str = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    char *equal = strstr(str, "=");
    if (!equal || equal == str)
        return NULL;
    char *val = equal + 1;
    int var_len;
    struct ast_node *node = new_node(NODE_ASSIGNMENT);
    if (*(equal - 1) == '+')
    {
        --equal;
        node->value.assignment_value->type = AS_PLUS_EQUAL;
    }
    else
        node->value.assignment_value->type = AS_EQUAL;
    var_len = equal - str;
    node->value.assignment_value->var = calloc(var_len + 1, sizeof (char));
    memcpy(node->value.assignment_value->var, str, var_len);
    node->value.assignment_value->value =
        calloc(strlen(val) + 1, sizeof (char));
    strcpy(node->value.assignment_value->value, val);
    return node;
}

struct ast_node *parse_prefix()
{
    struct parsable_line *line = get_line_struct();
    int save = line->index;
    struct ast_node *a = parse_assignement_node();
    if (a)
        return a;
    line->index = save;
    a = parse_redirection();
    return a;
}
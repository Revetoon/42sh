/* parse_rule_if.c */

# include "parse_rule_if.h"
# include "parse_compound_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

static struct ast_node *parse_else_clause()
{
    if (accept(TK_ELSE))
    {
        struct ast_node *n = parse_compound_list();
        return n;
    }
    else if (accept(TK_ELIF))
    {
        struct ast_node *n = parse_compound_list();
        if (!accept(TK_THEN))
        {
            free_ast(n);
            return NULL;
        }
        struct ast_node *m = parse_compound_list();
        if (!m)
        {
            free_ast(n);
            return NULL;
        }
        struct ast_node *o = parse_else_clause();

        struct ast_node *node = new_node(NODE_IF);
        node->value.if_value->conditions = n;
        node->value.if_value->then_node = m;
        node->value.if_value->else_node = o;
        return node;
    }
    else
        return NULL;
}

struct ast_node *parse_rule_if()
{
    if (!accept(TK_IF))
        return NULL;
    struct ast_node *n = parse_compound_list();
    if (!accept(TK_THEN))
    {
        free_ast(n);
        return NULL;
    }
    struct ast_node *m = parse_compound_list();
    struct ast_node *o = parse_else_clause();
    if (!accept(TK_FI))
    {
        free_ast(n);
        free_ast(m);
        free_ast(o);
        return NULL;
    }

    struct ast_node *node = new_node(NODE_IF);
    node->value.if_value->conditions = n;
    node->value.if_value->then_node = m;
    node->value.if_value->else_node = o;
    return node;
}
/* parse_fundec.c */

# include <stdlib.h>
# include <string.h>

# include "parse_fundec.h"
# include "parse_shell_command.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

struct ast_node *parse_fundec()
{
    struct parsable_line *line = get_line_struct();
    accept(TK_FUNCTION);
    char *name = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    if (!accept(TK_OPENPAREN))
        return NULL;
    if (!accept(TK_CLOSEPAREN))
        return NULL;
    accept_backreturns();
    struct ast_node *n = parse_shell_command();
    if (!n)
        return NULL;
    struct ast_node *fun = new_node(NODE_FUNCTION);
    fun->value.function_value->name =
        calloc(strlen(name) + 1, sizeof (char));
    strcpy(fun->value.function_value->name, name);
    fun->value.function_value->command_list = n;
    return fun;
}
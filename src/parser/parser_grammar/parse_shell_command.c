/* parse_shell_command.c */

# include "parse_shell_command.h"
# include "parse_compound_list.h"
# include "parse_rule_for.h"
# include "parse_rule_while.h"
# include "parse_rule_until.h"
# include "parse_rule_case.h"
# include "parse_rule_if.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

static struct ast_node *parse_shell_command_parenthesis()
{
    if (!accept(TK_OPENPAREN))
        return NULL;

    struct ast_node *n = parse_compound_list();
    if (!n)
        return NULL;
    if (!accept(TK_CLOSEPAREN))
    {
        free_ast(n);
        return NULL;
    }

    return n;
}

static struct ast_node *parse_shell_command_bracket()
{
    if (!accept(TK_OPENBRACKET))
        return NULL;

    struct ast_node *n = parse_compound_list();
    if (!n)
        return NULL;
    if (!accept(TK_CLOSEBRACKET))
    {
        free_ast(n);
        return NULL;
    }

    return n;
}

struct ast_node *parse_shell_command()
{
    struct parsable_line *line = get_line_struct();
    int save = line->index;
    struct ast_node *n = parse_shell_command_bracket();
    if (n)
        return n;
    line->index = save;
    n = parse_shell_command_parenthesis();
    if (n)
        return n;
    line->index = save;
    n = parse_rule_for();
    if (n)
        return n;
    line->index = save;
    n = parse_rule_while();
    if (n)
        return n;
    line->index = save;
    n = parse_rule_until();
    if (n)
        return n;
    line->index = save;
    n = parse_rule_case();
    if (n)
        return n;
    line->index = save;
    n = parse_rule_if();
    if (n)
        return n;
    return NULL;
}
/* parse_rule_case.c */

# include <stdlib.h>
# include <string.h>

# include "parse_rule_case.h"
# include "parse_compound_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

static struct case_condition *parse_case_item()
{
    struct parsable_line *line = get_line_struct();
    accept(TK_OPENPAREN);
    size_t count = count_items();
    char **s = calloc(count, sizeof (char*));
    int i = 0;
    int save = line->index;
    while (1)
    {
        char *c = line->words[line->index];
        if (!accept(GENERIC))
        {
            line->index = save;
            break;
        }
        s[i] = calloc(strlen(c) + 1, sizeof (char));
        strcpy(s[i], c);
        ++i;
        save = line->index;
        if (!accept(TK_PIPE))
            break;
    }
    if (!accept(TK_CLOSEPAREN))
    {
        for (int j = 0; j < i; ++j)
            free(s[j]);
        free(s);
        return NULL;
    }
    accept_backreturns();
    save = line->index;
    struct ast_node *n = parse_compound_list();
    if (!n)
        line->index = save;
    struct case_condition *cond = calloc(1, sizeof (struct case_condition));
    cond->conditions = s;
    cond->nb_conditions = i;
    cond->do_group = n;
    return cond;
}

static struct case_condition *parse_case_clause(int *count)
{
    struct parsable_line *line = get_line_struct();
    *count = 0;
    struct case_condition *c = parse_case_item();
    ++*count;
    struct case_condition *last = c;
    struct case_condition *res = c;
    int save = line->index;
    if (!c)
        return NULL;
    while (1)
    {
        save = line->index;
        if (!accept(TK_COLONCOLON))
            break;
        accept_backreturns();

        c = parse_case_item();
        if (!c)
        {
            line->index = save;
            break;
        }
        last->next = c;
        last = c;
        c->next = NULL;
        ++*count;
    }
    accept(TK_COLONCOLON);
    accept_backreturns();
    return res;
}

struct ast_node *parse_rule_case()
{
    struct parsable_line *line = get_line_struct();
    if (!accept(TK_CASE))
        return NULL;
    char *s = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    accept_backreturns();
    if (!accept(TK_IN))
        return NULL;
    accept_backreturns();
    int save = line->index;
    int count = 0;
    struct case_condition *n = parse_case_clause(&count);
    if (!n)
        line->index = save;
    if (!accept(TK_ESAC))
    {
        free_case_condition(n);
        return NULL;
    }
    struct ast_node *node = new_node(NODE_CASE);
    node->value.case_value->variable = s;
    node->value.case_value->cases = n;
    node->value.case_value->nb_case = count;
    return node;
}
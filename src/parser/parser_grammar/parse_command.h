/**
** @file
*/
# ifndef PARSE_COMMAND_H
# define PARSE_COMMAND_H
# include <stdbool.h>
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_command(bool invert);

# endif

/**
** @file
*/
# ifndef PARSE_SHELL_COMMAND_H
# define PARSE_SHELL_COMMAND_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_shell_command();

# endif

/* parse_redirection.c */

# include <stdlib.h>
# include <string.h>
# include <ctype.h>

# include "parse_redirection.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

static int atoi_file_desc(char *s)
{
    size_t i = 0;
    for (; s[i]; ++i)
    {
        if (!isdigit(s[i]))
            return -1;
    }
    int res = atoi(s);
    if (res > 1023)
        return -1;
    return res;
}

struct ast_node *parse_redirection()
{
    struct parsable_line *line = get_line_struct();
    char *io = line->words[line->index];
    int right_fd = 1;
    if (accept(GENERIC))
    {
        right_fd = atoi_file_desc(io);
        if (right_fd < 0)
            return NULL;
    }

    // Only those 2 are parsed for any redirection
    char *redir = line->words[line->index];
    if (!accept(TK_MORE) && !accept(TK_LESS))
        return NULL;

    char *w = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    struct ast_node *node = new_node(NODE_REDIRECTION);
    node->value.redirection_value->type = get_redir_type(redir);
    node->value.redirection_value->ionumber = right_fd;
    node->value.redirection_value->right =
        calloc(strlen(w) + 1, sizeof (char));
    strcpy(node->value.redirection_value->right, w);
    return node;
}
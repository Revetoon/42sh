/* parse_simple_command.c */

# include <stdlib.h>
# include <string.h>

# include "parse_simple_command.h"
# include "parse_prefix.h"
# include "parse_redirection.h"
# include "parse_element.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

struct ast_node *parse_simple_command(bool invert)
{
    struct parsable_line *line = get_line_struct();
    // parse prefix
    char *e = 0;
    struct ast_node *redir = NULL;
    int save = line->index;
    struct ast_node *prefix = parse_prefix();
    if (prefix)
    {
        struct ast_node *last = prefix;
        struct ast_node *p = last;
        while (p)
        {
            save = line->index;
            p = parse_prefix();
            last->next = p;
            last = p;
        }
        line->index = save;
    }
    else
        line->index = save;

    size_t count = count_args();
    redir = parse_redirection();
    if (!redir)
    {
        line->index = save;
        e = parse_element();
        // if no prefix & no element: quit
        if (!e && !prefix)
            return NULL;
    }
    struct ast_node *n = new_node(NODE_SIMPLE_COMMAND);
    struct ast_node *node = n;
    n->value.simple_command_value->argc = 0;
    n->value.simple_command_value->invert = invert;
    n->value.simple_command_value->prefix = prefix;
    if (!e)
        return n;
    n->value.simple_command_value->argv = calloc(count + 1, sizeof (char*));
    while (1)
    {
        if (redir)
        {
            node = add_redirection(node, redir);
        }
        else
        {
            n->value.simple_command_value->argv
            [n->value.simple_command_value->argc] =
            calloc(strlen(e) + 1, sizeof (char));
            strcpy(n->value.simple_command_value->argv
                [n->value.simple_command_value->argc], e);
            ++n->value.simple_command_value->argc;
        }
        save = line->index;
        redir = parse_redirection();
        if (!redir)
        {
            line->index = save;
            e = parse_element();
            if (!e)
                break;
        }
    }
    line->index = save;
    return node;
}
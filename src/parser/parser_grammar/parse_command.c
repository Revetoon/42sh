/* parse_command.c */

# include "parse_command.h"
# include "parse_shell_command.h"
# include "parse_redirection.h"
# include "parse_simple_command.h"
# include "parse_fundec.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

static struct ast_node *parse_command_shell()
{
    struct ast_node *n = parse_shell_command();
    if (!n)
        return NULL;
    struct ast_node *node = n;
    while (1)
    {
        struct ast_node *redirect = parse_redirection();
        if (!redirect)
            break;
        node = add_redirection(n, redirect);
        if (!node)
            break;
        n = node;
    }
    return n;
}

static struct ast_node *parse_command_fundec()
{
    struct ast_node *n = parse_fundec();
    if (!n)
        return NULL;
    struct ast_node *node = n;
    while (1)
    {
        struct ast_node *redirect = parse_redirection();
        if (!redirect)
            break;
        node = add_redirection(n, redirect);
        if (!node)
            break;
        n = node;
    }
    return n;
}

struct ast_node *parse_command(bool invert)
{
    struct parsable_line *line = get_line_struct();
    int save = line->index;
    struct ast_node *n = parse_simple_command(invert);
    if (n)
    {
        return n;
    }
    line->index = save;
    n = parse_command_shell();
    if (n)
    {
        return n;
    }
    line->index = save;
    n = parse_command_fundec();
    if (n)
    {
        return n;
    }
    line->index = save;
    return NULL;
}
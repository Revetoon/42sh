/* parse_compound_list.c */

# include "parse_compound_list.h"
# include "parse_and_or.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

struct ast_node *parse_compound_list()
{
    struct parsable_line *line = get_line_struct();
    int save = line->index;
    accept_backreturns();
    struct ast_node *n = parse_and_or();
    struct ast_node *m = NULL;
    if (!n)
        return NULL;
    struct ast_node *last = n;
    while (1)
    {
        save = line->index;
        if (!accept(TK_COLON)
            && !accept(TK_AMP)
            && !accept(TK_BACKRETURN))
            break;
        accept_backreturns();
        m = parse_and_or();
        if (!m)
        {
            line->index = save;
            break;
        }
        last->next = m;
        last = m;
    }
    line->index = save;
    if (accept(TK_AMP))
    {
        n->end_type = END_DELIM_AND;
        accept_backreturns();
    }
    else if (accept(TK_COLON))
    {
        n->end_type = END_DELIM_DEFAULT;
        accept_backreturns();
    }
    else if (accept(TK_BACKRETURN))
    {
        accept_backreturns();
    }
    return n;
}
/* parse_input.c */

# include "parse_input.h"
# include "parse_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

struct ast_node *parse_input()
{
    struct parsable_line *line = get_line_struct();
    if (accept(TK_BACKRETURN))
    {
        line->return_val = RET_DONE;
        return NULL;
    }
    struct ast_node *n = parse_list();
    if (!n)
        return NULL;
    if (accept(TK_BACKRETURN))
    {
        if (line->return_val == RET_PENDING)
            line->return_val = RET_DONE;
    }
    else
    {
        line->return_val = RET_PENDING;
        return NULL;
    }
    return n;
}
/* parser.c */

# include <stdlib.h>
# include <string.h>

# include "parse_input.h"
# include "parser.h"
# include "lexer.h"
# include  "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "interactive.h"

# include <stdio.h>

struct ast_node *parser(char *l, struct environment *env)
{
    struct ast_node *n = NULL;
    struct parsable_line *line = NULL;
    do {
        line = lexer(l);
        if (line)
        {
            init_line(line);
            n = parse_input();
        }

        if (!line || line->return_val == RET_PENDING)
        {
            l = append_next_line(l);
            if (!l)
                break;
            free_line(line);
            line = NULL;
        }
    } while (!line || line->return_val == RET_PENDING);

    if (!n)
    {
        if (line && line->err)
        {
            fprintf(stderr, "%s\n", line->err);
            env->previous_return = 1;
        }
    }

    // Free line
    free_line(line);
    if (l)
        free(l);

    return n;
}
/**
** @file
*/
# ifndef PARSE_DO_GROUP_H
# define PARSE_DO_GROUP_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_do_group();

# endif

/**
** @file
*/
# ifndef PARSE_COMPOUND_LIST_H
# define PARSE_COMPOUND_LIST_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_compound_list();

# endif

/* parse_do_group.c */

# include "parse_do_group.h"
# include "parse_compound_list.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

struct ast_node *parse_do_group()
{
    if (!accept(TK_DO))
        return NULL;
    struct ast_node *n = parse_compound_list();
    if (!accept(TK_DONE))
    {
        free_ast(n);
        return NULL;
    }
    return n;
}
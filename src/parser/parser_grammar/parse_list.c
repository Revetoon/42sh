/* parse_list.c */

# include "parse_list.h"
# include "parse_and_or.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"

struct ast_node *parse_list()
{
    struct ast_node *n = parse_and_or();
    struct ast_node *node = NULL;
    if (!n)
        return NULL;
    node = n;
    while (n)
    {
        if (accept(TK_COLON))
            node->end_type = END_DELIM_DEFAULT;
        else if (accept(TK_AMP))
            node->end_type = END_DELIM_AND;
        else
            break;
        n = parse_and_or();
        node->next = n;
    }
    if (accept(TK_COLON))
        node->end_type = END_DELIM_DEFAULT;
    else if (accept(TK_AMP))
        node->end_type = END_DELIM_AND;
    return node;
}
/* parse_rule_for.c */

# include <stdlib.h>
# include <string.h>

# include "parse_rule_for.h"
# include "parse_do_group.h"
# include "parser.h"
# include "ast.h"
# include "parser_line_helper.h"
# include "ast_helpers.h"
# include "parser_functions.h"
# include "parser_free.h"

struct ast_node *parse_rule_for()
{
    struct parsable_line *line = get_line_struct();
    if (!accept(TK_FOR))
        return NULL;
    char *i = line->words[line->index];
    if (!accept(GENERIC))
        return NULL;
    struct ast_node *n = new_node(NODE_FOR);
    if (!accept(TK_COLON))
    {
        accept_backreturns();
        if (!accept(TK_IN))
        {
            free_ast(n);
            return NULL;
        }
        n->value.for_value->index = calloc(strlen(i) + 1, sizeof (char));
        strcpy(n->value.for_value->index, i);
        size_t count = count_args();
        size_t j = 0;
        n->value.for_value->list = calloc(count, sizeof (char*));
        while (1)
        {
            char *c = line->words[line->index];
            if (!accept(GENERIC))
                break;
            n->value.for_value->list[j] =
                calloc(strlen(c) + 1, sizeof (char));
            strcpy(n->value.for_value->list[j], c);
            ++j;
        }
        n->value.for_value->list_size = j;
        if (!accept(TK_COLON) && !accept(TK_BACKRETURN))
        {
            free_ast(n);
            return NULL;
        }
    }
    accept_backreturns();
    struct ast_node *do_grp = parse_do_group();
    if (!do_grp)
    {
        free_ast(n);
        return NULL;
    }
    n->value.for_value->do_group = do_grp;
    return n;
}
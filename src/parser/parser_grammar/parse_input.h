/**
** @file
*/
# ifndef PARSE_INPUT_H
# define PARSE_INPUT_H
# include "ast.h"
/**
 * parse an ast_node
 * @return the parsed node of NULL if an error occured
 */
struct ast_node *parse_input();

# endif

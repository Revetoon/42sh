/* parser_line_helper.c */

# include <stdlib.h>

# include "parser_line_helper.h"

static struct parsable_line *line = NULL;

void init_line(struct parsable_line *l)
{
    line = l;
}

struct parsable_line *get_line_struct()
{
    return line;
}

void free_line(struct parsable_line *l)
{
    if (!l)
        return;
    for (int i = 0; i < l->size; ++i)
    {
        free(l->words[i]);
    }
    free(l->tokens);
    free(l->words);
    free(l->err);
    free(l);
}
/* ast_print.c */

# include <stdio.h>
# include <string.h>

# include "ast_print.h"

size_t read_ast_print(struct ast_node *ast, struct str_vector *v);

char *escape_string(char *str)
{
    int i = 0;
    int count = 0;
    for (; str[i]; ++i)
    {
        count += str[i] == '"';
        count += str[i] == '\n';
    }
    char *st = calloc(i + count + 1, sizeof (char));
    int c = 0;
    for (i = 0; str[i]; ++i)
    {
        if (str[i] == '\n')
        {
            st[c++] = '\\';
            st[c++] = 'n';
            continue;
        }
        if (str[i] == '"')
            st[c++] = '\\';
        st[c++] = str[i];
    }
    return st;
}

size_t print_add_label(struct str_vector *v, char *str)
{
    size_t ret = v->index;
    char *str_esc = escape_string(str);
    char *s = calloc (strlen(str) + 40, sizeof (char));
    sprintf(s, "lbl_%ld [label=\"%s\"];\n",
        v->index, str_esc);
    str_vector_add(v, s);
    free(s);
    free(str_esc);
    return ret;
}

void link_nodes(struct str_vector *v, size_t from, size_t to, char *msg)
{
    if (msg && *msg)
    {
        char *s = calloc(strlen(msg) + 70, sizeof (char));
        sprintf(s, "lbl_%ld -> lbl_%ld [label=\"%s\"];\n",
            from, to, msg);
        str_vector_add(v, s);
        free(s);
    }
    else
    {
        char *s = calloc(50, sizeof (char));
        sprintf(s, "lbl_%ld -> lbl_%ld\n",
        from, to);
        str_vector_add(v, s);
        free(s);
    }
}


size_t print_assignement(struct ast_node *ast, struct str_vector *v)
{
    char *my_res = calloc(strlen(ast->value.assignment_value->var) +
                       strlen(ast->value.assignment_value->value) +
                       20, sizeof (char));
    sprintf(my_res, "%s%s%s",
        ast->value.assignment_value->var,
        ast->value.assignment_value->type == AS_EQUAL ? "=" : "+=",
        ast->value.assignment_value->value);
    size_t ret = print_add_label(v, my_res);
    free(my_res);
    return ret;
}

size_t print_simple_command(struct ast_node *ast, struct str_vector *v)
{
    char *cmd;
    char *res = NULL;
    if (ast->value.simple_command_value->argc > 0)
    {
        cmd = ast->value.simple_command_value->argv[0];
        res = calloc(strlen(cmd) + 16, sizeof (char));
        sprintf(res, "cmd:%s %s\\nargc:%d",
            ast->value.simple_command_value->invert ? "!" : "",
            cmd,
            ast->value.simple_command_value->argc);
    }
    else
    {
        res = calloc(11, sizeof (char));
        sprintf(res, "Empty cmd");
    }
    size_t ret = print_add_label(v, res);
    struct ast_node *n = ast->value.simple_command_value->prefix;
    while (n)
    {
        size_t child = print_assignement(n, v);
        link_nodes(v, ret, child, "prefix");
        n = n->next;
    }
    free(res);
    return ret;
}

size_t print_and(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "&&");
    size_t left = read_ast_print(ast->value.and_value->left, v);
    size_t right = read_ast_print(ast->value.and_value->right, v);
    link_nodes(v, ret, left, "left");
    link_nodes(v, ret, right, "right");
    return ret;
}

size_t print_or(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "||");
    size_t left = read_ast_print(ast->value.and_value->left, v);
    size_t right = read_ast_print(ast->value.and_value->right, v);
    link_nodes(v, ret, left, "left");
    link_nodes(v, ret, right, "right");
    return ret;
}

size_t print_pipe(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "|");
    size_t left = read_ast_print(ast->value.and_value->left, v);
    size_t right = read_ast_print(ast->value.and_value->right, v);
    link_nodes(v, ret, left, "left");
    link_nodes(v, ret, right, "right");
    return ret;
}

size_t print_function(struct ast_node *ast, struct str_vector *v)
{
    char *s = calloc(strlen(ast->value.function_value->name) + 20,
        sizeof (char));
    sprintf(s, "function:\\n%s", ast->value.function_value->name);
    size_t ret = print_add_label(v, s);

    struct ast_node *n = ast->value.function_value->command_list;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, NULL);
        n = n->next;
    }
    free(s);
    return ret;
}

size_t print_redirection(struct ast_node *ast, struct str_vector *v)
{
    char *redir[7] = {
        ">", "<", ">>", ">&", "<&", ">|", "<>"
    };
    char *s = calloc(strlen(ast->value.redirection_value->right) + 17,
        sizeof (char));
    sprintf(s, "%d %s %s",
        ast->value.redirection_value->ionumber,
        redir[ast->value.redirection_value->type],
        ast->value.redirection_value->right);
    size_t ret = print_add_label(v, s);
    if (ast->value.redirection_value->child)
    {
        size_t child = read_ast_print(ast->value.redirection_value->child, v);
        link_nodes(v, ret, child, NULL);
    }
    free(s);
    return ret;
}

size_t print_while(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "while");

    struct ast_node *n = ast->value.while_value->condition;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "cond");
        n = n->next;
    }
    n = ast->value.while_value->do_group;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "do");
        n = n->next;
    }
    return ret;
}

size_t print_until(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "until");

    struct ast_node *n = ast->value.until_value->condition;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "cond");
        n = n->next;
    }
    n = ast->value.until_value->do_group;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "do");
        n = n->next;
    }
    return ret;
}

size_t print_case_condition(struct case_condition *cond,
                            struct str_vector *v)
{
    char *s = calloc(20, sizeof (char));
    sprintf(s, "Condition\\ncount:%ld",
        cond->nb_conditions);

    size_t ret = print_add_label(v, s);

    struct ast_node *n = cond->do_group;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "do");
        n = n->next;
    }
    free(s);
    return ret;
}

size_t print_case(struct ast_node *ast,
                  struct str_vector *v)
{
    char *s = calloc(strlen(ast->value.case_value->variable) + 30,
        sizeof (char));
    sprintf(s, "Case:\\n%s\\nnb conditions:%ld",
        ast->value.case_value->variable,
        ast->value.case_value->nb_case);
    size_t ret = print_add_label(v, s);

    size_t count = 0;
    struct case_condition *c = ast->value.case_value->cases;
    while (count < ast->value.case_value->nb_case)
    {
        size_t child = print_case_condition(c, v);
        char *line = calloc(strlen(c->conditions[0]) + 10,
            sizeof (char));
        sprintf(line, "%s%s",
            c->conditions[0],
            c->nb_conditions > 1 ? "|...": "");
        link_nodes(v, ret, child, line);
        free(line);
        c = c->next;
        ++count;
    }
    free(s);
    return ret;
}

size_t print_for(struct ast_node *ast, struct str_vector *v)
{
    char *s = calloc(strlen(ast->value.for_value->index) + 20,
        sizeof (char));
    sprintf(s, "For:\\n%s\\nlist size:%ld",
        ast->value.for_value->index,
        ast->value.for_value->list_size);
    size_t ret = print_add_label(v, s);

    struct ast_node *n = ast->value.for_value->do_group;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "do");
        n = n->next;
    }
    free(s);
    return ret;
}

size_t print_if(struct ast_node *ast, struct str_vector *v)
{
    size_t ret = print_add_label(v, "If");
    struct ast_node *n = ast->value.if_value->conditions;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "cond");
        n = n->next;
    }
    n = ast->value.if_value->then_node;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "then");
        n = n->next;
    }
    n = ast->value.if_value->else_node;
    while (n)
    {
        size_t child = read_ast_print(n, v);
        link_nodes(v, ret, child, "else");
        n = n->next;
    }
    return ret;
}



size_t read_ast_print(struct ast_node *ast, struct str_vector *v)
{
    f_print_fct fct_list[12] =
    {
        print_assignement,
        print_simple_command,
        print_and,
        print_or,
        print_pipe,
        print_function,
        print_redirection,
        print_while,
        print_until,
        print_case,
        print_for,
        print_if
    };

    if (!ast)
        return 0;

    return fct_list[ast->type](ast, v);
}



void ast_print(struct ast_node *ast)
{
    struct str_vector v;
    init_str_vector(&v);

    // begining
    str_vector_add(&v, "digraph ast {\n");

    read_ast_print(ast, &v);

    // end
    str_vector_add(&v, "}");

    FILE *f = fopen(AST_PRINT_FILENAME, "w+");
    if (!f)
    {
        fprintf(stderr, "42sh: ast-print: can't open for writing \"%s\""
            , AST_PRINT_FILENAME);
        return;
    }
    fprintf(f, "%s\n", v.data);

    fclose(f);

    // free
    free(v.data);
}
/* str_vector.c */

 # include <string.h>
 # include <stdlib.h>

# include "str_vector.h"


void double_vect_size(struct str_vector *v)
{
    size_t n = v->capacity * 2;
    v->data = realloc(v->data, n * sizeof (char));
    v->capacity = n;
}

void init_str_vector(struct str_vector *v)
{
    v->index = 0;
    v->size = 0;
    v->capacity = 8;
    v->data = calloc(v->capacity, sizeof (char));
}

void free_str_vector_data(struct str_vector *v)
{
    free(v->data);
}

void str_vector_add(struct str_vector *v, const char *str)
{
    size_t len = strlen(str);
    while (v->size + len >= v->capacity)
        double_vect_size(v);
    strcpy(v->data + v->index, str);
    v->index += len;
    v->size += len;
}
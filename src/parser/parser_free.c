/* parser_free.c */

# include <stdlib.h>

# include "parser_free.h"
# include "parser.h"

static void free_ast_node(struct ast_node *ast)
{
    if (!ast)
        return;
    free_ast(ast->next);
    free(ast);
}

static void free_assignement(struct ast_node *ast)
{
    if (!ast || !ast->value.assignment_value)
        return;
    free(ast->value.assignment_value->var);
    free(ast->value.assignment_value->value);
    free(ast->value.assignment_value);
    free_ast_node(ast);
}

static void free_simple_command(struct ast_node *ast)
{
    if (!ast || !ast->value.simple_command_value)
        return;
    if (ast->value.simple_command_value->argc > 0)
    {
        for (int i = 0; ast->value.simple_command_value->argv[i]; ++i)
            free(ast->value.simple_command_value->argv[i]);
        free(ast->value.simple_command_value->argv);
    }
    free_ast(ast->value.simple_command_value->prefix);
    free(ast->value.simple_command_value);
    free_ast_node(ast);
}

static void free_and(struct ast_node *ast)
{
    if (!ast || !ast->value.and_value)
        return;
    free_ast(ast->value.and_value->left);
    free_ast(ast->value.and_value->right);
    free(ast->value.and_value);
    free_ast_node(ast);
}

static void free_or(struct ast_node *ast)
{
    if (!ast || !ast->value.or_value)
        return;
    free_ast(ast->value.or_value->left);
    free_ast(ast->value.or_value->right);
    free(ast->value.or_value);
    free_ast_node(ast);
}

static void free_pipe(struct ast_node *ast)
{
    if (!ast || !ast->value.pipe_value)
        return;
    free_ast(ast->value.pipe_value->left);
    free_ast(ast->value.pipe_value->right);
    free(ast->value.pipe_value);
    free_ast_node(ast);
}

static void free_function(struct ast_node *ast)
{
    if (!ast || !ast->value.function_value)
        return;
    free(ast->value.function_value->name);
    free_ast(ast->value.function_value->command_list);
    free(ast->value.function_value);
    free_ast_node(ast);
}

static void free_redirection(struct ast_node *ast)
{
    if (!ast || !ast->value.redirection_value)
        return;
    free_ast(ast->value.redirection_value->child);
    free(ast->value.redirection_value->right);
    free(ast->value.redirection_value);
    free_ast_node(ast);
}

static void free_while(struct ast_node *ast)
{
    if (!ast || !ast->value.while_value)
        return;
    free_ast(ast->value.while_value->condition);
    free_ast(ast->value.while_value->do_group);
    free(ast->value.while_value);
    free_ast_node(ast);
}

static void free_until(struct ast_node *ast)
{
    if (!ast || !ast->value.until_value)
        return;
    free_ast(ast->value.until_value->condition);
    free_ast(ast->value.until_value->do_group);
    free(ast->value.until_value);
    free_ast_node(ast);
}

void free_case_condition(struct case_condition *cond)
{
    if (!cond)
        return;
    free_case_condition(cond->next);
    for (size_t i = 0; i < cond->nb_conditions; ++i)
        free(cond->conditions[i]);
    free(cond->conditions);
    free_ast(cond->do_group);
    free(cond);
}

static void free_case(struct ast_node *ast)
{
    if (!ast || !ast->value.case_value)
        return;
    free_case_condition(ast->value.case_value->cases);
    free(ast->value.case_value->variable);
    free(ast->value.case_value);
    free_ast_node(ast);
}

static void free_for(struct ast_node *ast)
{
    if (!ast || !ast->value.for_value)
        return;
    for (size_t i = 0; i < ast->value.for_value->list_size; ++i)
        free(ast->value.for_value->list[i]);
    free(ast->value.for_value->list);
    free(ast->value.for_value->index);
    free_ast(ast->value.for_value->do_group);
    free(ast->value.for_value);
    free_ast_node(ast);
}

static void free_if(struct ast_node *ast)
{
    if (!ast || !ast->value.if_value)
        return;
    free_ast(ast->value.if_value->conditions);
    free_ast(ast->value.if_value->then_node);
    free_ast(ast->value.if_value->else_node);
    free(ast->value.if_value);
    free_ast_node(ast);
}

void free_ast(struct ast_node *ast)
{
    f_free_fct fct_list[12] =
    {
        free_assignement,
        free_simple_command,
        free_and,
        free_or,
        free_pipe,
        free_function,
        free_redirection,
        free_while,
        free_until,
        free_case,
        free_for,
        free_if
    };

    if (!ast)
        return;

    fct_list[ast->type](ast);
}
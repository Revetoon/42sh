/* ast_helpers.c */

# include <stdlib.h>
# include <string.h>

# include "ast_helpers.h"

static void new_node3(enum node_type type, struct ast_node *node)
{
    if (type == NODE_SIMPLE_COMMAND)
        node->value.simple_command_value =
            calloc(1, sizeof (struct simple_command_node));
    else if (type == NODE_ASSIGNMENT)
        node->value.assignment_value =
            calloc(1, sizeof (struct assignment_node));
}

static void new_node2(enum node_type type, struct ast_node *node)
{
    if (type == NODE_AND)
        node->value.and_value = calloc(1, sizeof (struct and_node));
    else if (type == NODE_OR)
        node->value.or_value = calloc(1, sizeof (struct or_node));
    else if (type == NODE_PIPE)
        node->value.pipe_value = calloc(1, sizeof (struct pipe_node));
    else if (type == NODE_FUNCTION)
        node->value.function_value = calloc(1, sizeof (struct function_node));
    else if (type == NODE_REDIRECTION)
        node->value.redirection_value =
            calloc(1, sizeof (struct redirection_node));
    else if (type == NODE_WHILE)
        node->value.while_value = calloc(1, sizeof (struct while_node));
    else if (type == NODE_UNTIL)
        node->value.until_value = calloc(1, sizeof (struct until_node));
    else if (type == NODE_CASE)
        node->value.case_value = calloc(1, sizeof (struct case_node));
    else if (type == NODE_FOR)
        node->value.for_value = calloc(1, sizeof (struct for_node));
    else if (type == NODE_IF)
        node->value.if_value = calloc(1, sizeof (struct if_node));
    else
        new_node3(type, node);
}

struct ast_node *new_node(enum node_type type)
{
    struct ast_node *node = calloc(1, sizeof (struct ast_node));
    node->type = type;
    new_node2(type, node);
    return node;
}

struct ast_node *add_redirection(struct ast_node *node, struct ast_node *redirect)
{
    if (!redirect || redirect->type != NODE_REDIRECTION)
        return NULL;
    redirect->value.redirection_value->child = node;
    return redirect;
}

enum redirection_type get_redir_type(char *redir)
{
    if (strcmp(">", redir) == 0)
        return REDIRECTION_TYPE_OUTPUT;
    if (strcmp("<", redir) == 0)
        return REDIRECTION_TYPE_INPUT;
    if (strcmp(">>", redir) == 0)
        return REDIRECTION_TYPE_APPEND;
    if (strcmp(">&", redir) == 0)
        return REDIRECTION_TYPE_OUTPUT_DUP;
    if (strcmp("<&", redir) == 0)
        return REDIRECTION_TYPE_INPUT_DUP;
    if (strcmp(">|", redir) == 0)
        return REDIRECTION_TYPE_OUTPUT_PIPE;
    if (strcmp("<>", redir) == 0)
        return REDIRECTION_TYPE_INPUT_OUTPUT;
    return 0;
}
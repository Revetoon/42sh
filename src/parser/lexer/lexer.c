/* lexer.c */

# include <stdlib.h>
# include <string.h>

# include "parser.h"
# include "ast.h"
# include "token_type.h"

void dismiss_comments(char *str)
{
    int i = 0;
    int squote_c = 0;
    int dquote_c = 0;
    int space = 1;
    for (; str[i]; ++i)
    {
        if (str[i] == '\\')
        {
            ++i;
            continue;
        }
        if (squote_c)
        {
            squote_c = str[i] != '\'';
            continue;
        }
        if (dquote_c)
        {
            dquote_c = str[i] != '"';
            continue;
        }
        if (str[i] == '"')
            dquote_c = 1;
        else if (str[i] == '\'')
            squote_c = 1;
        if (str[i] == ' ')
            space = 1;
        else if (str[i] == '#' && space)
        {
            str[i] = '\n'; // End
            if (str[i + 1])
                str[i + 1] = 0;
            return;
        }
        else
            space = 0;
    }
}

size_t next_sepa(char *str)
{
    // j: 10
    char *charlst = "><&|;(){}\n";
    size_t count = 0;
    size_t space = 0;
    for (; str[count]; ++count)
    {
        if (!space && str[count] == '\\')
        {
            ++count;
            continue;
        }
        if (str[count] == ' ')
            space = 1;
        else if (space)
            return count - 1;
        else
            for (size_t j = 0; j < 10; ++j)
                if (charlst[j] == str[count])
                    return count == 0 ? count : count - 1;
    }
    return count - 1;
}
size_t quick_count_words(char *line)
{
    size_t count = 0;
    while (*line)
    {
        size_t off = next_sepa(line);
        ++count;
        line += off + 1;
    }
    return count;
}

e_token *lex_all(char **words, size_t size)
{
    e_token *result = calloc(size + 1, sizeof (e_token));
    size_t index = 0;
    if (size == 0)
        result[index++] = TK_EOF;
    for (size_t i = 0; words[i]; ++i)
    {
        result[index++] = gettoktype(words[i]);
    }
    return result;
}

int match_quotes(char *str, char quote)
{
    if (*str != quote)
        return 0;
    size_t len = 1;
    for (; str[len]; ++len)
    {
        if (str[len] == '\\')
            ++len;
        else if (str[len] == quote)
            return len + 1;
    }
    return -1;
}

int no_trailing_whitespaces(char *line, size_t len)
{
    int i = len - 1;
    for (; i >= 0 && (line[i] == ' '); --i)
        continue;
    return i + 1;
}

size_t word_len(char *str)
{
    if (strncmp(str, ">>", 2) == 0)
        return 2;
    if (strncmp(str, "<<-", 3) == 0)
        return 3;
    if (strncmp(str, "<<", 2) == 0)
        return 2;
    if (strncmp(str, ">&", 2) == 0)
        return 2;
    if (strncmp(str, "<&", 2) == 0)
        return 2;
    if (strncmp(str, "<>", 2) == 0)
        return 2;
    if (strncmp(str, ">|", 2) == 0)
        return 2;
    if (strncmp(str, ";;", 2) == 0)
        return 2;
    if (strncmp(str, "||", 2) == 0)
        return 2;
    if (strncmp(str, "&&", 2) == 0)
        return 2;
    return 0;
}

char *create_generic_word(char *line, int *offset, int *ret)
{
    *offset = 1;
    *ret = -1;
    int off = next_sepa(line);
    int index = 0;
    while (index <= off)
    {
        if (line[index] == '\\')
        {
            index+= 2;
            continue;
        }
        int match = match_quotes(line + index, '"');
        if (!match)
            match = match_quotes(line + index, '\'');
        if (!match)
        {
            ++index;
            continue;
        }
        if (match == -1)
            return NULL; // ret = -1
        index += match - 1;
        off = index + next_sepa(line + index);
        ++index;
    }
    *ret = 1;
    int word_sz = no_trailing_whitespaces(line, index);
    if (!word_sz)
        return NULL; // ret = 1
    *offset = index;
    char *word = calloc(word_sz + 1, sizeof (char));
    memcpy(word, line, word_sz * sizeof (char));
    *ret = 0;
    return word; // ret = 0
}

char *create_word(char *line, int *offset, int *ret)
{
    *ret = 0;
    *offset = 1;
    char *word = NULL;

    int word_sz = 0;
    word_sz = word_len(line);
    if (!word_sz)
    {
        return create_generic_word(line, offset, ret);
    }
    if (!word_sz)
    {
        *ret = 1;
        return NULL;
    }
    word = calloc(word_sz + 1, sizeof (char));
    memcpy(word, line, word_sz * sizeof (char));
    *offset = word_sz;
    return word;
}

void free_words(char **words, int len)
{
    for (int i = 0; i < len; ++i)
        free(words[i]);
    free(words);
}

char **separate_into_words(char *line, size_t approx_size, int *size)
{
    char **words = calloc((approx_size + 1), sizeof (char*));
    size_t index = 0;
    int off = 0;
    int ret = 0;
    while (*line)
    {
        words[index] = create_word(line, &off, &ret);
        if (!words[index] && ret == -1)
        {
            free_words(words, index - 1);
            return NULL;
        }
        line += off;
        if (ret == 0)
            ++index;
    }
    *size = index;
    return words;
}

struct parsable_line *lexer(char *line)
{
    dismiss_comments(line);
    size_t approx_size = quick_count_words(line);
    int size = 0;
    char **words = separate_into_words(line, approx_size, &size);
    if (!words)
        return NULL;
    e_token *tokens = lex_all(words, size);
    struct parsable_line *l = calloc(1, sizeof (struct parsable_line));
    l->tokens = tokens;
    l->words = words;
    l->size = size;
    return l;
}
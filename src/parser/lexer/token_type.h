/**
** @file token_type.h
** @brief token type
*/
# ifndef TOKEN_TYPE_H
# define TOKEN_TYPE_H

/**
 * returns the type of token the given string represent
 * @param  elm the string to tokenize
 * @return     returns a tkoen type
 */
e_token gettoktype(const char *elm);


# endif
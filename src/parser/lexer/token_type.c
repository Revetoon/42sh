/* token_type.c */

# include <string.h>

# include "parser.h"

e_token gettoktype_2(const char *elm, e_token tok)
{
    char *map_list2 = " &|;<>";
    e_token tok2 = 1;
    for (; map_list2[tok2]; ++tok2)
        if (map_list2[tok2] == *elm)
            break;
    if (tok == TK_AMP && tok2 == TK_AMP)
        return TK_AND;
    if (tok == TK_PIPE && tok2 == TK_PIPE)
        return TK_OR;
    if (tok == TK_COLON && tok2 == TK_COLON)
        return TK_COLONCOLON;
    return tok;
}

e_token gettoktype_command(const char *elm)
{
    size_t size = strlen(elm);
    char *map_list[] =
    {
        "if",
        "while",
        "until",
        "for",
        "case",
        "function",
        "in",
        "esac",
        "then",
        "fi",
        "do",
        "done",
        "elif",
        "else"
    };
    int i = 0;
    for (; i < 14; ++i)
        if (strlen(map_list[i]) == size
            && strcmp(map_list[i], elm) == 0)
            break;
    if (i >= 14)
        return GENERIC;
    return TK_IF + i;
}

e_token gettoktype(const char *elm)
{
    if (!*elm)
        return TK_EOF;
    char *map_list = " &|;!<>(){}\n#";
    e_token tok = 1;
    for (; map_list[tok]; ++tok)
        if (map_list[tok] == *elm)
            break;
    if (!map_list[tok])
        return gettoktype_command(elm);
    return gettoktype_2(elm + 1, tok);
}
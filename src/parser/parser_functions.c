/* parser_functions.c */

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "ast.h"
# include "parser_functions.h"
# include "parser_line_helper.h"

static void syntax_err()
{
    static int max_index = 0;
    static char** line_pt = NULL;
    struct parsable_line *line = get_line_struct();
    if (line->tokens[line->index] == TK_BACKRETURN
        || line->tokens[line->index] == TK_EOF)
        return;
    if (line->words != line_pt)
    {
        line_pt = line->words;
        max_index = line->index;
    }
    else if (line->index <= max_index)
        return;
    max_index = line->index;
    if (line->err)
        free(line->err);
    line->err = calloc(strlen(line->words[line->index]) + 45,
        sizeof (char));
    sprintf(line->err, "42sh: syntax error near unexpected token '%s'",
        line->words[line->index]);
}

int accept(e_token tk)
{
    struct parsable_line *line = get_line_struct();
    if (line->index >= line->size)
        return tk == TK_EOF;
    if (line->tokens[line->index] == tk)
    {
        if (line->tokens[line->index] == TK_BACKRETURN
            && line->index == line->size - 1)
            line->return_val = RET_PENDING;
        if (line->tokens[line->index] != TK_EOF)
            line->index++;
        return 1;
    }
    syntax_err();
    return 0;
}


int count_args()
{
    struct parsable_line *line = get_line_struct();
    int count = 0;
    for (; line->index + count < line->size; ++count)
    {
        if (line->tokens[line->index + count] != GENERIC)
            break;
    }
    return count;
}

int count_items()
{
    struct parsable_line *line = get_line_struct();
    int count = 0;
    for (; line->index + count < line->size; ++count)
    {
        if (line->tokens[line->index + count] != GENERIC
            && line->tokens[line->index + count] != TK_PIPE)
            break;
    }
    return count;
}

void accept_backreturns()
{
    struct parsable_line *line = get_line_struct();
    if (line->tokens[line->index] == TK_BACKRETURN
        && line->index == line->size - 1)
        line->return_val = RET_PENDING;
    while (line->tokens[line->index] == TK_BACKRETURN)
    {
        ++line->index;
    }
}
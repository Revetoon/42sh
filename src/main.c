/**
** @file main.c
** @brief Main 42sh
*/

# include <stdio.h>
# include <stdlib.h>
# include <pwd.h>
# include <readline/history.h>
# include <string.h>
# include <unistd.h>
# include <readline/history.h>

# include "exec_ast.h"
# include "buffer.h"
# include "options_parser.h"
# include "execution.h"

int main(int argc, char **argv)
{
  const char *history_file_name = "/.42sh_history";
  const char *rc_file_name_one = "/etc/42shrc";
  const char *rc_file_name_two = "/.42shrc";

  struct environment *env = init_environment();
  int arg_index = 1;
  if (parse_bash_options(argc, (const char**)argv, &arg_index, env))
  {
    int return_value = env->previous_return;
    free_environment(env);
    return return_value;
  }

  struct passwd *pw = getpwuid(getuid());
  struct buffer buffer = {.size = 0, .buffer = NULL};

  if (!env->norc)
  {
    add_buffer(&buffer, pw->pw_dir, strlen(pw->pw_dir));
    add_buffer(&buffer, rc_file_name_two, strlen(rc_file_name_two));
    execut_file(rc_file_name_one, env);
    execut_file(buffer.buffer, env);
    empty_buffer(&buffer);
  }

  if (arg_index != argc)
  {
    return execut_file(argv[arg_index], env);
  }

  add_buffer(&buffer, pw->pw_dir, strlen(pw->pw_dir));
  add_buffer(&buffer, history_file_name, strlen(history_file_name));
  read_history(buffer.buffer);
  int res = forty_two_sh(env, STDIN_FILENO);
  write_history(buffer.buffer);
  clear_history();
  free_environment(env);
  empty_buffer(&buffer);
  return res;
}

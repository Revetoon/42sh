/**
** @file expand_simple_command.c
** @brief Expand simple command
** @authors xu_f
*/
# include <assert.h>
# include "expansions.h"

void expand_assignment(struct ast_node *ast, struct environment *env,
                       char *(*func)(char*, struct environment*))
{
  assert(ast);
  assert(ast->type == NODE_ASSIGNMENT);
  struct assignment_node *node = ast->value.assignment_value;
  node->value = func(node->value, env);
}

void expand_simple_command(struct ast_node *ast, struct environment *env,
                           char *(*func)(char*, struct environment*))
{
  assert(ast);
  assert(ast->type == NODE_SIMPLE_COMMAND);
  struct simple_command_node *cmd = ast->value.simple_command_value;
  for (struct ast_node *ptr = cmd->prefix; ptr; ptr = ptr->next)
    expand_assignment(ptr, env, func);
  if (cmd->argv)
  {
    for (int i = 1; i < cmd->argc; ++i)
    {
      cmd->argv[i] = func(cmd->argv[i], env);
    }
  }
}
/**
** @file variable_expansion.c
** @brief Variable expansion
** @authors xu_f
*/

#include <assert.h>
#include <ctype.h>
# include "expansions.h"
# include "expand.h"

static char *variable_expansion(char *c, struct environment *env);

static char *get_variable_in_bracket(char *token, char *string, char **save_ptr)
{
  char *var_end = NULL;
  for (var_end = token + 1; var_end && *var_end != '}'; ++var_end)
  {
    if (*var_end == ' ')
    {
      token[-1] = '$';
      fprintf(stderr, "42sh: %s: bad substitution\n", string);
      return NULL;
    }
  }
  if (!var_end || *var_end != '}')
  {
    token[-1] = '$';
    return NULL;
  }
  *var_end = '\0';
  *save_ptr = var_end[1] ? var_end + 1 : NULL;
  return token + 1;
}

static char *get_variable(char *token, char *string, char **save_ptr,
                          char *save_delim)
{
  if (*token == '{')
  {
    *save_delim = '\0';
    return get_variable_in_bracket(token, string, save_ptr);
  }
  bool positional_param = isdigit(*token) != 0;
  for (char *ptr = token + 1; *ptr; ++ptr)
  {
    if (*ptr == ' ' || *ptr == '$' || positional_param || *ptr == '"')
    {
      *save_delim = *ptr;
      *ptr = '\0';
      *save_ptr = ptr + 1;
      return token;
    }
  }
  *save_ptr = NULL;
  return token;
}

static char *reassemble(char *c, char *end, char *value, char delim)
{
  size_t before_len = strlen(c);
  size_t add_space = delim ? 1 : 0;
  size_t after_len = end ? strlen(end) : 0;
  size_t value_len = strlen(value);
  size_t total_len = before_len + after_len + value_len + add_space;
  if (total_len == 0)
  {
    return calloc(1, sizeof(char));
  }
  char *new = malloc(total_len + 1);
  if (!new)
  {
    fprintf(stderr, "42sh: Memory exhausted\n");
    return c;
  }
  if (add_space)
    new[before_len + value_len] = delim;
  memcpy(new, c, before_len);
  memcpy(new + before_len + value_len + add_space,
         end,
         after_len);
  memcpy(new + before_len, value, value_len);
  new[total_len] = '\0';
  return new;
}

static char *get_before(char *c, char **token)
{
  for (char *ptr = c; *ptr; ++ptr)
  {
    if (*ptr == '$')
    {
      *ptr = '\0';
      *token = ptr + 1;
      return c;
    }
  }
  *token = NULL;
  return c;
}
static void get_next(char **end, char *save_delim, bool *free_end,
                     struct environment *env)
{
  if (end)
  {
    (*end)[-1] = *save_delim;
    char *new_end = variable_expansion(*end - 1, env);
    (*end)[-1] = '\0';
    if (*end - 1 != new_end)
    {
      *end = new_end;
      *save_delim = 0;
      *free_end = true;
    }
  }
}
static char *variable_expansion(char *c, struct environment *env)
{
  assert(c);
  if (*c == '\0' || *c == '\'')
    return c;
  char *token = NULL;
  c = get_before(c, &token);
  if (!token)
    return c;
  if (*token == '\0' || *token == ' ')
  {
    token[-1] = '$';
    return c;
  }
  char *end = NULL;
  char save_delim = 0;
  bool free_end = false;
  char *variable = get_variable(token, c, &end, &save_delim);
  char *value = find_variable(variable, env);
  if (end)
    get_next(&end, &save_delim, &free_end, env);
  c = reassemble(c, end, value, save_delim);
  free(value);
  if (free_end)
    free(end);
  return c;
}

char *var_expand_wrapper(char *c, struct environment *env)
{
  char *new = variable_expansion(c, env);
  if (new != c)
    free(c);
  return new;
}

void expand_variable(struct ast_node *ast, struct environment *env)
{
  expand_ast(ast, env, var_expand_wrapper);
}
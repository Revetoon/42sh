/**
** @file expand_argv.c
** @brief Expand argument array
** @authors xu_f
*/

# include "expansions.h"

char *expand_star(const char *var, struct environment *env)
{
  (void)var;
  if (env->argc < 2)
    return strdup("");
  char *ifs = strdup(" ");
  if (env_var_isset(env, "IFS"))
    ifs = get_var_environment(env, "IFS");
  char *return_value = strdup(env->argv[1]);
  size_t size = strlen(return_value);
  for (int i = 2; i < env->argc; ++i)
  {
    char *tmp = env->argv[i];
    return_value[size] = ifs[0];
    size_t new_size = size + strlen(tmp) + 1;
    return_value = realloc(return_value, new_size);
    return_value[size + 1] = '\0';
    return_value = strcat(return_value, tmp);
    size = new_size;
  }
  free(ifs);
  return return_value;
}

char *expand_at(const char *var, struct environment *env)
{
  (void)var;
  if (env->argc < 2)
    return strdup("");
  char *return_value = strdup(env->argv[1]);
  size_t size = strlen(return_value);
  for (int i = 2; i < env->argc; ++i)
  {
    char *tmp = env->argv[i];
    size_t tmp_len = strlen(tmp);
    return_value[size] = '"';
    size_t new_size = size + tmp_len + 2;
    return_value = realloc(return_value, new_size);
    strcpy(return_value + size + 1, tmp);
    size = new_size - 1;
  }
  return return_value;
}
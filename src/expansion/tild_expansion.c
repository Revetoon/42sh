/**
** @file tild_expansion.c
** @brief Tildes expansion
** @authors xu_f
*/

#include <assert.h>
# include "expansions.h"
# include "expand.h"

char *tild_expansion(char *c, struct environment *env)
{
  assert(c);
  if (*c != '~')
    return c;
  char *path = NULL;
  size_t tild_len = 2;
  if (c[1] == '+')
    path = get_var_environment(env, "PWD");
  else if (c[1] == '-')
    path = get_var_environment(env, "OLDPWD");
  else
  {
    path = get_var_environment(env, "HOME");
    tild_len--;
  }
  size_t len = strlen(path);
  size_t string = strlen(c) - tild_len;
  char *new = realloc(c, len + string + 1);
  if (!new)
  {
    fprintf(stderr, "42sh: memory exhausted\n");
    free(path);
    return c;
  }
  memmove(new + len, new + tild_len, string + 1);
  memcpy(new, path, len);
  free(path);
  return new;
}

void expand_tild(struct ast_node *ast, struct environment *env)
{
  expand_ast(ast, env, tild_expansion);
}
/**
** @file quote_removal.c
** @brief Quotes removal
** @authors xu_f
*/

#include <assert.h>
# include "expansions.h"

static char *single_quote_remove(char *string)
{
  size_t size = strlen(string);
  memmove(string, string + 1, size - 2);
  string[size - 2] = '\0';
  return string;
}

static char *backslash_remove(char *string)
{
  size_t len = strlen(string);
  for (size_t i = 0; i < len; ++i)
  {
    if (string[i] == '"')
      string[i] = ' ';
    if (string[i] == '\\')
    {
      memmove(string + i, string + i + 1, len - i);
      --len;
    }
  }
  return string;
}

static size_t my_quote_strlen(char *string)
{
  size_t len = 0;
  for (; *string && *string != '"'; ++string)
  {
    if (*string == '\\')
    {
      ++string;
      ++len;
    }
    ++len;
  }
  return len;
}

static char *assignment_double_quote_strip_null(char *string)
{
  for (; *string != '"'; ++string)
  {
    if (*string == '\\')
      ++string;
    else if (*string == '"')
      *string = ' ';
  }
  return string;
}

static char *double_quote_remove_backslash(char *string)
{
  size_t len = strlen(string);
  for (size_t i = 0; i < len; ++i)
  {
    if (string[i] == '\\' && string[i + 1] &&
      (string[i + 1] == '"' || string[i + 1] == '\\'))
    {
      memmove(string + i, string + i + 1, len - i);
      --len;
    }
  }
  return string;
}

static void double_quote_expand(char *string, int *argindex,
                                 int *argc, char ***argv)
{
  assert(string && *string && *string == '"');
  char **new_tab = calloc(1, sizeof (char*));
  size_t size = 0;
  for (char *ptr = string + 1; *ptr; ++ptr)
  {
    size_t len = my_quote_strlen(ptr);
    new_tab = realloc(new_tab, (size + 1) * sizeof(char *));
    new_tab[size] = calloc(len + 1, sizeof (char));
    memcpy(new_tab[size++], ptr, len);
    ptr += len;
  }
  int new_size = *argc + (int)size;
  int diff = new_size - *argc - 1;
  *argv = realloc(*argv, (*argc + size) * sizeof (char*));
  for (int i = *argc; i > *argindex; --i)
    (*argv)[i + diff] = (*argv)[i];
  for (size_t i = 0; i < size; ++i, ++*argindex)
    (*argv)[*argindex] = double_quote_remove_backslash(new_tab[i]);
  *argc = new_size - 1;
  free(string);
  free(new_tab);
  --*argindex;
}

void quotes_assignment(struct ast_node *ast)
{
  assert(ast);
  assert(ast->type == NODE_ASSIGNMENT);
  struct assignment_node *node = ast->value.assignment_value;
  char *value = node->value;
  switch (*value)
  {
    case '\0':
      break;
    case '\'':
      single_quote_remove(value);
      break;
    case '\"':
      value = assignment_double_quote_strip_null(value);
      value = double_quote_remove_backslash(value);
      value = single_quote_remove(value);
      break;
    default:
      value = backslash_remove(value);
      break;
  }
  node->value = value;
}

void quotes_simple_command(struct ast_node *ast)
{
  assert(ast);
  assert(ast->type == NODE_SIMPLE_COMMAND);
  struct simple_command_node *cmd = ast->value.simple_command_value;
  if (!cmd->argv || cmd->argc == 0)
    return;
  int argindex = 0;
  for (; argindex < cmd->argc; ++argindex)
  {
    char *value = cmd->argv[argindex];
    switch (*value)
    {
      case '\0':
        break;
      case '\'':
        single_quote_remove(value);
        break;
      case '\"':
        double_quote_expand(value, &argindex, &cmd->argc, &cmd->argv);
        break;
      default:
        backslash_remove(value);
        break;
    }
  }
}

void quote_removal(struct ast_node *ast)
{
  if (ast->type == NODE_ASSIGNMENT)
    quotes_assignment(ast);
  else if (ast->type == NODE_SIMPLE_COMMAND)
    quotes_simple_command(ast);
}

void string_quote_removal(int *argc, char ***argv)
{
  int argindex = 0;
  for (; argindex < *argc; ++argindex)
  {
    char *value = (*argv)[argindex];
    switch (*value)
    {
      case '\0':
        break;
      case '\'':
        single_quote_remove(value);
        break;
      case '\"':
        double_quote_expand(value, &argindex, argc, argv);
        break;
      default:
        backslash_remove(value);
        break;
    }
  }
}
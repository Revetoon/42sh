/**
** @file expansions.h
** @brief Expansion wrapper header
** @authors xu_f
*/

#ifndef INC_42SH_EXPANSIONS_H
#define INC_42SH_EXPANSIONS_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include "ast.h"
# include "exec_ast.h"
# include "environment.h"

/**
** @brief Expand assignment node
** @param ast ast node of type NODE_ASSIGNMENT
** @param env environment
** @param func function to be applied on string values
*/
void expand_assignment(struct ast_node *ast, struct environment *env,
                       char *(*func)(char*, struct environment*));

/**
** @brief Expand simple command node
** @param ast ast node of type NODE_SIMPLE_COMMAND
** @param env environment
** @param func function to be applied on string values
*/
void expand_simple_command(struct ast_node *ast, struct environment *env,
                           char *(*func)(char*, struct environment*));

/**
** @brief Expand ast node wrapper
** @param ast ast node
** @param env environment
** @param func function to be applied on string values
*/
void expand_ast(struct ast_node *ast, struct environment *env,
                char *(*func)(char*, struct environment*));

/**
** @brief Variable expansion: find variable in environment and special cases
** @param var name/identifier of the variable
** @param env environment
** @return value of the variable
*/
char *find_variable(char *var, struct environment *env);

/**
** @brief Expand $@
** @param var should be "@"
** @param env environment
** @return expanded $@ separate variables with a '"'
*/
char *expand_at(const char *var, struct environment *env);

/**
** @brief Expand $*
** @param var should be "*"
** @param env environment
** @return expanded $*
** separate variable with IFS first character or ' ' if not set
*/
char *expand_star(const char *var, struct environment *env);

/**
** @brief Quote and backslash removal wrapper
** @param ast ast node
*/
void quote_removal(struct ast_node *ast);

/**
** @brief Expand variable wrapper
** @param c string to be expanded
** @param env environment
** @return expanded variable
*/
char *var_expand_wrapper(char *c, struct environment *env);

/**
** @brief Expand tildes
** @param c string to be expanded
** @param env environment
** @return expanded string
*/
char *tild_expansion(char *c, struct environment *env);

/**
** @brief Remove all quotes, double, simple and backslash.
** @param argc number of arguments in argv
** @param argv array of arguments ended with NULL
**
** Has to be called after variable expansion to replace " set by expansion
** of $@ to its correct value.
*/
void string_quote_removal(int *argc, char ***argv);

#endif /* !INC_42SH_EXPANSIONS_H */
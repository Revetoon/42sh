/**
** @file shell_var_expand.c
** @brief Shell variable expand
** @authors xu_f
*/

# include <stdlib.h>
# include "expansions.h"

static char *var_previous(const char *var, struct environment *env)
{
  (void)var;
  char *value = calloc(4, sizeof (char));
  sprintf(value, "%d", env->previous_return & (511));
  return value;
}

static char *var_nbvar(const char *var, struct environment *env)
{
  (void)var;
  char *return_value = calloc(12, sizeof(char));
  sprintf(return_value, "%d", env->argc - 1);
  return return_value;
}

static char *positional_arg(int index, struct environment *env)
{
  if (index >= env->argc)
    return strdup("");
  return strdup(env->argv[index]);
}

static char *var_cmd_name(const char *var, struct environment *env)
{
  (void)var;
  return strdup(env->argv[0]);
}

struct svar_wrapper
{
  char *name;
  char *(*func)(const char*, struct environment*);
};

static struct svar_wrapper shell_vars[] =
  {
    {"?", var_previous},
    {"#", var_nbvar},
    {"0", var_cmd_name},
    {"@", expand_at},
    {"*", expand_star},
    {NULL, NULL}
  };

char *find_variable(char *var, struct environment *env)
{
  if (!var || !*var)
    return strdup("");
  int positional = atoi(var);
  if (positional > 0)
    return positional_arg(positional, env);
  for (struct svar_wrapper *ptr = shell_vars; ptr->name; ++ptr)
  {
    if (strcmp(ptr->name, var) == 0)
      return ptr->func(var, env);
  }
  return get_var_environment(env, var);
}
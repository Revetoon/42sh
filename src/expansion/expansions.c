/**
** @file expansions.c
** @brief Expansion wrapper
** @authors xu_f
*/

#include <assert.h>
# include "expand.h"
# include "expansions.h"
struct expand_wrapper
{
  enum node_type type;
  void (*function)(struct ast_node *ast, struct environment *env,
               char *(*func)(char*, struct environment*));
};

static struct expand_wrapper expand_asts[] =
  {
    {NODE_SIMPLE_COMMAND, expand_simple_command},
    {NODE_ASSIGNMENT, expand_assignment},
    {0, NULL}
  };

void expand_ast(struct ast_node *ast, struct environment *env,
                       char *(*func)(char*, struct environment*))
{
  for (struct expand_wrapper *ptr = expand_asts; ptr->function; ++ptr)
  {
    if (ptr->type == ast->type){
      ptr->function(ast, env, func);
      return;
    }
  }
}

void expand_all(struct ast_node *ast, struct environment *env)
{
  assert(ast);
  assert(env);
  expand_tild(ast, env);
  expand_variable(ast, env);
  quote_removal(ast);
}

void string_all_expand(int *argc, char ***argv, struct environment *env)
{
  for (int i = 0; i < *argc; ++i)
  {
    (*argv)[i] = tild_expansion((*argv)[i], env);
    (*argv)[i] = var_expand_wrapper((*argv)[i], env);
  }
  string_quote_removal(argc, argv);
}
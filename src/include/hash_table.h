/**
** @file hash_table.h
** @brief Hash table header
** @authors xu_f
*/

#ifndef INC_42SH_HASH_TABLE_H
#define INC_42SH_HASH_TABLE_H

# include <stdlib.h>
# include <stdint.h>

/**
** @struct pair
** @brief Pair stored in hash table structure
*/
struct pair
{
  uint32_t hkey;
  /*!< hashed value of key */
  char *key;
  /*!< key */
  void *value;
  /*!< value of the key */
  struct pair *next;
  /*!< collision array next value */
};

/**
** @struct htable
** @brief Hash table structure
*/
struct htable
{
  size_t size;
  /*!< number of elements in the hash table */
  size_t capacity;
  /*!< capacity of the array */
  struct pair **tab;
  /*!< array of pair structure */
};

/**
** @brief Create hash table
** @param capacity capacity of the hash table
** @return Initialized and allocated hash table
*/
struct htable *create_htable(size_t capacity);

/**
** @brief Access hash table with key
** @param htable hash table
** @param key key
** @return pair structure of founded key
*/
struct pair *access_htable(struct htable *htable, char *key);

/**
** @brief Add a value in hash table
** @param htable hash table
** @param key key of the value
** @param value value
** @return 1 if succeeded
** @return 0 if failed
*/
int add_htable(struct htable *htable, char *key, void *value);

/**
** @brief Remove a hash table value
** @param htable hash table
** @param key key of the value
*/
void remove_htable(struct htable *htable, char *key);

/**
** @brief Free hash table
** @param htable hash table
*/
void free_htable(struct htable *htable);

/*
** Extension
*/

/**
** @brief Copy hash table
** @param htable hash table
** @param data_copy function to copy the values in hash table
** @return Allocated and initialized copy of the hash table
*/
struct htable *copy_htable(struct htable *htable,
                           void *(*data_copy)(void*));

/**
** @brief Get all value of a hash table
** @param htable hash table
** @param size_out allocated size_t that will contain the return array size
** @return allocated array of all values in the hash table
*/
void **get_all_value_htable(struct htable *htable, size_t *size_out);

#endif /* !INC_42SH_HASH_TABLE_H */
/**
** @file options_parser.h
** @brief Option parser header
** @authors xu_f
*/
#ifndef INC_42SH_OPTIONS_PARSER_H
#define INC_42SH_OPTIONS_PARSER_H

# include "main_42sh.h"
# include "environment.h"
/**
** @brief Parse bash and shell options
** @param argc number of argument in argv
** @param argv argument array
** @param arg_index current index of the array
** @return true if 42sh must exit
*/
bool parse_bash_options(int argc, const char **argv,
                       int *arg_index_out, struct environment *env);

#endif /* INC_42SH_OPTIONS_PARSER_H */
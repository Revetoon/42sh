/**
** @file parser.h
** @brief parser
*/
# ifndef PARSER_H
# define PARSER_H

# include "environment.h"

# define DELIM " "

/**
** @enum e_token
** @brief Enumeration of token types
*/
typedef enum
{
    GENERIC = 0,
    TK_AMP,           /*!< & */
    TK_PIPE,          /*!< | */
    TK_COLON,         /*!< ; */
    TK_EXCLA,         /*!< ! */
    TK_LESS,          /*!< < */
    TK_MORE,          /*!< > */
    TK_OPENPAREN,     /*!< ( */
    TK_CLOSEPAREN,    /*!< ) */
    TK_OPENBRACKET,   /*!< { */
    TK_CLOSEBRACKET,  /*!< } */
    TK_BACKRETURN,    /*!< \n */
    TK_IF,            /*!< if */
    TK_WHILE,         /*!< while */
    TK_UNTIL,         /*!< until */
    TK_FOR,           /*!< for */
    TK_CASE,          /*!< case */
    TK_FUNCTION,      /*!< function */
    TK_IN,             /*!< in */
    TK_ESAC,          /*!< esac */
    TK_THEN,          /*!< then */
    TK_FI,            /*!< fi */
    TK_DO,            /*!< do */
    TK_DONE,          /*!< done */
    TK_ELIF,          /*!< elif */
    TK_ELSE,          /*!< else */
    TK_EOF,           /*!< \0 */
    TK_AND,           /*!< && */
    TK_COLONCOLON,    /*!< ;; */
    TK_OR,            /*!< || */
    TK_LESSLESS,      /*!< << */
    TK_MOREMORE,      /*!< >> */
    TK_LESLESSMINUS,  /*!< <<- */
    TK_MOREAND,       /*!< >& */
    TK_LESSAND,       /*!< <& */
    TK_MOREPIPE,      /*!< >| */
    TK_LESSMORE       /*!< <> */
} e_token;

/**
** @enum parse_ret_type
** @brief Enumeration of return types for the parser
*/
enum parse_ret_type
{
    RET_DONE = 0,
    RET_PENDING,
    RET_ERR
};

/**
** @struct parsable_line
** @brief a structure containing the elements to parse a string into an ast.
*/
struct parsable_line
{
    char **words;
    e_token *tokens;
    int index;
    int size;
    enum parse_ret_type return_val;
    char *err;
};

/**
 * The parser function, will create an ast based on the input line given.
 * @param  l   the input line
 * @param  env the environment
 * @return     the ast
 */
struct ast_node *parser(char *l, struct environment *env);

# endif
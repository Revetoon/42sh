/**
** @file 42sh.h
** @brief 42sh header
** @authors xu_f
*/

#ifndef INC_42SH_42SH_H
#define INC_42SH_42SH_H

# define PROJECT_VERSION 1.0f

# include <stdbool.h>

#endif //INC_42SH_42SH_H
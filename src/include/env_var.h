/**
** @file environment.h
** @brief Environment variable header
** @authors xu_f
*/

#ifndef INC_42SH_ENV_VAR_H
#define INC_42SH_ENV_VAR_H

# include <stdbool.h>
# include "hash_table.h"
# include "environment.h"
/**
** @struct env_var
** @brief Environment variable structure
*/
struct env_var
{
  char *key;
  /*!< name of the variable */
  char *value;
  /*!< value of the variable */
  bool exported;
  /*!< exported attribute of the variable */
  bool readonly;
  /*!< readonly attribute of the variable */
};

/**
** @brief Initialize env_var structure
** @param key name of the variable
** @param value value of the variable
** @param exported exported attribute of the variable
** @param readonly readonly attribute of the variable
** @return Allocated and initialized env_var structure
*/
struct env_var *env_var_init(const char *key, const char *value,
                             bool exported, bool readonly);

/**
** @brief Set value in variable
** @param env_var variable structure
** @param new_value new value of variable
** @return 0 on success 1 on error
**
** Possible error are:
**   -set readonly variable
*/
int env_var_set(struct env_var *env_var, const char *new_value);

/**
** @brief Duplicated variable structure
** @param var variable to be duplicated
** @return Initialized and allocated duplicate of variable
*/
struct env_var *env_var_dup(struct env_var *var);

/**
** @brief Free env_var structure
** @param var variable structure to be freed
*/
void env_var_free(struct env_var *var);

/**
** @brief Get env_var from hash_table in env
** @param env environment
** @param key name of the variable
** @return variable structure of name key, NULL if not found
*/
struct env_var *get_env_var(struct environment *env, char *key);

#endif /* !INC_42SH_ENV_VAR_H */
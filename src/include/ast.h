/**
** @file ast.h
** @brief Ast structures
*/
# ifndef AST_H
# define AST_H

# include <stdbool.h>
# include <stddef.h>
/* external structs*/
/* end external structs */

/**
** @enum end_delim
** @brief Enumeration of end delimiter
*/
enum end_delim
{
  END_DELIM_DEFAULT = 0,
  /*!< ending with ';' or '\\n' */
  END_DELIM_AND
  /*!< ending with '&' */
};

/**
** @enum node_type
** @brief Enumeration of ast node type
*/
enum node_type
{
  NODE_ASSIGNMENT = 0,
  /*!< type assignment */
  NODE_SIMPLE_COMMAND,
  /*!< type simple command */
  NODE_AND,
  /*!< type and */
  NODE_OR,
  /*!< type of */
  NODE_PIPE,
  /*!< type pipe */
  NODE_FUNCTION,
  /*!< type function */
  NODE_REDIRECTION,
  /*!< type redirection */
  NODE_WHILE,
  /*!< type while */
  NODE_UNTIL,
  /*!< type until */
  NODE_CASE,
  /*!< type case */
  NODE_FOR,
  /*!< type for */
  NODE_IF
  /*!< type if */
};

/**
** @union node_value
** @brief Union of ast node values
*/
union node_value
{
  struct assignment_node *assignment_value;
  /*!< assignment node value */
  struct simple_command_node *simple_command_value;
  /*!< simple command node value */
  struct and_node *and_value;
  /*!< and node value */
  struct or_node *or_value;
  /*!< or node value */
  struct pipe_node *pipe_value;
  /*!< pipe node value */
  struct function_node *function_value;
  /*!< function node value */
  struct redirection_node *redirection_value;
  /*!< redirection node value */
  struct while_node *while_value;
  /*!< while node value */
  struct until_node *until_value;
  /*!< until node value */
  struct case_node *case_value;
  /*!< case node value */
  struct for_node *for_value;
  /*!< for node value */
  struct if_node *if_value;
  /*!< if node value */
};

/**
** @enum assignment_type
** @brief Enumeration of assignment type
*/
enum assignment_type
{
  AS_EQUAL = 0,
  /*!< equal assignment '=' */
  AS_PLUS_EQUAL
  /*!< plus equal assignment '+=' */
};

/**
** @struct assignment node
** @brief Structure of assignment node
*/
struct assignment_node
{
  enum assignment_type type;
  /*!< type of assignment */
  char *var;
  /*!< name of the variable */
  char *value;
  /*!< value of the variable */
};

/**
** @struct simple_command_node
** @brief Structure of simple command node
*/
struct simple_command_node
{
  int argc;
  /*!< number of argument */
  char **argv;
  /*!< array of argument terminated by NULL */
  bool invert;
  /*!< true if output is inverted */
  struct ast_node *prefix;
  /*!< linked list of NODE_ASSIGNMENTS */
};

/**
** @struct ast_node
** @brief Structure of ast node
*/
struct ast_node
{
  enum end_delim end_type;
  /*!< end type (enumeration of end type) */
  enum node_type type;
  /*!< node type (enumeration of node type) */
  union node_value value;
  /*!< node value (union of node_value) */
  struct ast_node *next;
  /*!< next ast node (linked list) */
};

/**
** @struct and_node
** @brief Structure of and node
*/
struct and_node
{
  struct ast_node *left;
  /*!< left ast node */
  struct ast_node *right;
  /*!< right ast node */
};

/**
** @struct or_node
** @brief Structure of or node
*/
struct or_node
{
  struct ast_node *left;
  /*!< left ast node */
  struct ast_node *right;
  /*!< right ast node */
};

/**
** @struct pipe_node
** @brief Structure of pipe node
*/
struct pipe_node
{
  struct ast_node *left;
  /*!< left ast node */
  struct ast_node *right;
  /*!< right ast node */
};

/**
** @struct function_node
** @brief Structure of function node
*/
struct function_node
{
  char *name;
  /*!< name of the function */
  struct ast_node *command_list;
  /*!< linked list of function body */
};

/**
** @enum redirection_type
** @brief Enumeration of redirection type
*/
enum redirection_type
{
  REDIRECTION_TYPE_OUTPUT = 0,
  /*!< redirection type output '>' */
  REDIRECTION_TYPE_INPUT,
  /*!< redirection type input '<' */
  REDIRECTION_TYPE_APPEND,
  /*!< redirection type append '>>' */
  REDIRECTION_TYPE_OUTPUT_DUP,
  /*!< redirection type output dup '>&' */
  REDIRECTION_TYPE_INPUT_DUP,
  /*!< redirection type input dup '<&' */
  REDIRECTION_TYPE_OUTPUT_PIPE,
  /*!< redirection type output pipe '>|' */
  REDIRECTION_TYPE_HERE_DOC,
  /*!< redirection type here_doc '<<' */
  REDIRECTION_TYPE_INPUT_OUTPUT
  /*!< redirection type input output '<>' */
};

/**
** @struct redirection_node
** @brief Structure of redirection node
*/
struct redirection_node
{
  enum redirection_type type;
  /*!< redirection type (enumeration of redirection type) */
  unsigned int ionumber;
  /*!< file descriptor number */
  char *right;
  /*!< right element of the redirection */
  struct ast_node *child;
  /*!< ast node to be executed after redirection
  (simple command or redirection) */
};

/**
** @struct while_node
** @brief Structure of while node
*/
struct while_node
{
  struct ast_node *condition;
  /*!< linked list of condition */
  struct ast_node *do_group;
  /*!< linked list of while body */
};

/**
** @struct until_node
** @brief Structure of until node
*/
struct until_node
{
  struct ast_node *condition;
  /*!< linked list of condition */
  struct ast_node *do_group;
  /*!< linked list of until body */
};

/**
** @struct case_condition
** @brief Structure of case condition
*/
struct case_condition
{
  char **conditions;
  /*!< array of conditions */
  size_t nb_conditions;
  /*!< size of conditions array */
  struct ast_node *do_group;
  /*!< linked list of case condition body */
  struct case_condition *next;
  /*!< next case_condition node */
};

/**
** @struct case_node
** @brief Structure of case node
*/
struct case_node
{
  char *variable;
  /*!< string to be tested in conditions */
  struct case_condition *cases;
  /*!< linked list of case conditions */
  size_t nb_case;
  /*!< number of case conditions */
};

/**
** @struct for_node
** @brief Structure of for node
*/
struct for_node
{
  char *index;
  /*!< name of the indexer variable ex: for i in ... (index = "i") */
  char **list;
  /*!< array of string values */
  size_t list_size;
  /*!< number of values */
  struct ast_node *do_group;
  /*!< linked list of for body */
};

/**
** @struct if_node
** @brief Structure of if node
*/
struct if_node
{
  struct ast_node *conditions;
  /*!< linked list of conditions */
  struct ast_node *then_node;
  /*!< linked list of then body */
  struct ast_node *else_node;
  /*!< linked list of else body */
};

# endif /* !AST_H */
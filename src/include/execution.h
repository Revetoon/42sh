/**
** \file execution.h
*/

#ifndef EXECUTION_H
# define EXECUTION_H

# include "environment.h"

/**
** \fn int forty_two_sh(struct environment *env, int fd_in)
** \brief Starts the module 42sh which manages the input, the parsing and the
          execution modules.
** \param env The environment information of the program.
** \param fd_in The file descriptore of the program input.
** \return Returns the output status of the program.
*/
int forty_two_sh(struct environment *env, int fd_in);

/**
** \fn int execut_file(const char *file_name, struct environment *env)
** \brief Interpret a shell command line file.
** \param file_name Name of the file to interpret.
** \param env The environment information of the program.
** \return Returns the output status of the program.
*/
int execut_file(const char *file_name, struct environment *env);

#endif /* !EXECUTION_H */

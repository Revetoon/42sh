/**
** @file ast_print.h
** @brief ast print
*/
# ifndef AST_PRINT_H
# define AST_PRINT_H

# include "ast.h"
# include "str_vector.h"
# include <stdlib.h>

# define AST_PRINT_FILENAME "ast.dot"

typedef size_t (*f_print_fct)(struct ast_node *ast, struct str_vector *v);

/**
 * Prints the given ast
 * @param ast the ast
 */
void ast_print(struct ast_node *ast);

# endif
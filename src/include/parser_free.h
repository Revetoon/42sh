/**
** @file parser_free.h
** @brief parser free
*/
# ifndef PARSER_FREE_H
# define PARSER_FREE_H

# include "ast.h"

typedef void (*f_free_fct)(struct ast_node *ast);

/**
 * Free the entire ast given in parameters
 * @param ast the root of the ast to free
 */
void free_ast(struct ast_node *ast);

/**
 * Free a structure case condition
 * @param cond the case condition to free
 */
void free_case_condition(struct case_condition *cond);


# endif
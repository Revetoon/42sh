/**
** @file environment.h
** @brief Environment header
** @authors xu_f
*/

#ifndef INC_42SH_ENVIRONMENT_H
#define INC_42SH_ENVIRONMENT_H

# include <stdbool.h>
# include "ast.h"

/**
** @struct environment
** @brief Structure of the environment
*/
struct environment
{
  int argc; /*!< Number of values in argv*/
  int previous_return; /*!< Previous return of a command ($?) */
  char **argv; /*!< Argument array of size argc + 1 terminated by null */
  struct htable *env_vars;
  /*!< hash_table containing all environment variables */
  struct htable *env_func;
  /*!< hash_table containing all environment functions */
  /* list of shell options */
  bool print_ast; /*!< shell option ast_print */
  bool norc; /*!< shell option loading resources files */
};

/**
** @brief Initialize environment structure
** @return Allocated and initialized struct environment
*/
struct environment *init_environment(void);

/**
** @brief Get environment variable
** @param env environment
** @param key name/identifier of the variable
** @return Allocated (string) copy of the variable (has to be free)
*/
char *get_var_environment(struct environment *env, char *key);

/**
** @brief Set environment variable
** @param env environment
** @param key name/identifier of the variable
** @param value value of the variable
** @param exported true if the variable is exported (add in environ)
** @param readonly true if the variable is readonly
**
** All string passed in argument are duplicated
*/
void set_var_environment(struct environment *env, char *key, char *value,
                         bool exported, bool readonly);

/**
** @brief Duplicated environment for export
** @param env environment
** @return Allocated environment containing all exported variable of env
*/
struct environment *export_environment(struct environment *env);

/**
** @brief Check if an environment variable is set
** @param env environment
** @param key name/identifier of the variable
** @return true if the variable is set
*/
bool env_var_isset(struct environment *env, char *key);

/**
** @brief Set environment function
** @param env environment
** @param ast ast_node of value function_node
**
** The function body is detached from the ast node and stored in the environment
** ast->value.function_node->command_list = NULL
*/
void set_func_environment(struct environment *env, struct ast_node *ast);

/**
** @brief Get environment function
** @param env environment
** @param key name/identifier of the function
** @return ast_node (command_list) of the function
**
** Do not modify the returned ast content, the ast is not duplicated
*/
struct ast_node *get_func_environment(struct environment *env, char *key);

/**
** @brief Free structure environment
** @param env environment
*/
void free_environment(struct environment *env);

/**
** @brief Set a variable to exported
** @param env environment
** @param var name/identifier of the variable
**
** If the variable is not set, it is created and set to exported
*/
void set_exported(struct environment *env, char *var);

/**
** @brief Remove the exported property of a variable
** @param env environment
** @param var name/identifier of the variable
**
** Noting is done if the variable is not set
*/
void remove_exported(struct environment *env, char *var);

/**
** @brief Add all variables in environ to env
** @param env environment
*/
void init_export_env(struct environment *env);

/**
** @brief Modify or create environment variable
** @param env environment
** @param key name/identifier of the variable
** @param value value of the variable
**
** If the variable is set, modify its value.
** Else set the variable to:
** -value = value
** -exported = false
** -readonly = false
**
** Like in set_var_environment all string given as argument are duplicated
*/
void modify_var_environment(struct environment *env, char *key, char *value);

#endif /* !INC_42SH_ENVIRONMENT_H */
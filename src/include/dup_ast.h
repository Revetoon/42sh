/**
** @file dup_ast.h
** @brief Duplicate ast for expansions
** @authors xu_f
*/

#ifndef INC_42SH_DUP_AST_H
#define INC_42SH_DUP_AST_H

# include <stdlib.h>
# include "ast.h"

/**
** @brief Free duplicated ast
** @param ast ast node to be free
*/
void free_dup_ast(struct ast_node *ast);

/**
** @brief Duplicate ast node for expansion
** @param ast ast node to be duplicated
** @return Duplicated ast, has to be freed with free_dup_ast
*/
struct ast_node *dup_ast_exec(struct ast_node *ast);

#endif /* !INC_42SH_DUP_AST_H */
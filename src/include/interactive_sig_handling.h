/**
** \file interactive_sig_handling.h
*/

#ifndef INTERACTIVE_SIG_HANDLING_H
# define INTERACTIVE_SIG_HANDLING_H

/**
** \fn void set_interactive_signal_handling(void)
** \brief Set all unwanted signals to have no effect.
*/
void set_interactive_signal_handling(void);

/**
** \fn void unset_interactive_signal_handling(void)
** \brief Set all unwanted signals back to default behaviour.
*/
void unset_interactive_signal_handling(void);

#endif /* !INTERACTIVE_SIG_HANDLING_H */
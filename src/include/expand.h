/**
** @file expand.h
** @brief Expand string header
** @authors xu_f
*/

#ifndef INC_42SH_EXPAND_H
#define INC_42SH_EXPAND_H

# include "ast.h"
# include "environment.h"

/**
** @brief Expand tildes
** @param ast ast_node
** @param env environment
*/
void expand_tild(struct ast_node *ast, struct environment *env);

/**
** @brief Expand variables
** @param ast ast_node
** @param env environment
*/
void expand_variable(struct ast_node *ast, struct environment *env);

/**
** @brief Call all expansions and quote removal on ast_node
** @param ast ast_node
** @param env environment
*/
void expand_all(struct ast_node *ast, struct environment *env);

/**
** @brief Call all expansions and quote removal on string array
** @param argc number of argument
** @param argv argument array
** @param env environment
*/
void string_all_expand(int *argc, char ***argv, struct environment *env);

#endif /* !INC_42SH_EXPAND_H */
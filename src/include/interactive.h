/**
** \file interactive.h
*/

#ifndef INTERACTIVE_H
# define INTERACTIVE_H

/**
** \fn char *get_next_line(int fd_in, int ps)
** \brief Return the next line of the programs input.
** \param fd_in File descriptor of the programs input.
** \param ps Which prompt to display, 1 for PS1 and 2 for PS2.
** \return Return the next line of the programs input.
*/
char *get_next_line(int fd_in, int ps);

/**
** \fn char *append_next_line(char *old)
** \brief Appends old to the new line of the program input.
** \param old String that will be concatenated to the new line.
** \return Returns the 'old' string to the new program input line.
*/
char *append_next_line(char *old);

#endif /* !INTERACTIVE_H */
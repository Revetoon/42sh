/**
** @file builtin.c
** @brief Builtins execution wrapper
** @authors xu_f
*/

# include "builtin.h"
# include "exec_ast.h"

/**
** @struct builtin wrapper
** @brief Wrapper for builtin execution
*/
struct builtin_wrapper
{
  char *name; /*!< name of the builtin */
  bool (*func)(struct simple_command_node*, struct command_args*);
  /*!< function of the builtin */
};

static struct builtin_wrapper builtins[] =
{
  {"exit", builtin_exit},
  {"env", builtin_env},
  {"cd", builtin_cd},
  {"export", builtin_export},
  {NULL, NULL}
};

bool execute_builtin(struct simple_command_node *cmd,
                     struct command_args *args, bool *return_out)
{
  assert(cmd);
  assert(cmd->argv);
  assert(cmd->argv[0]);
  for (struct builtin_wrapper *ptr = builtins; ptr->name; ++ptr)
  {
    if (strcmp(ptr->name, cmd->argv[0]) == 0)
    {
      *return_out = ptr->func(cmd, args);
      if (cmd->invert)
        args->env->previous_return = args->env->previous_return == 0;
      return true;
    }
  }
  return false;
}
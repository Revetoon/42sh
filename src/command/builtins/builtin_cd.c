/**
** @file builtin_cd.c
** @brief Builtin cd
** @authors xu_f
*/

# include <limits.h>
# include <stdio.h>
# include <unistd.h>
# include "builtin.h"

static void cd_old(struct command_args *args)
{
  if (!env_var_isset(args->env, "OLDPWD"))
  {
    fprintf(stderr, "42sh: cd: OLDPWD not set\n");
    args->env->previous_return = 2;
    return;
  }
  char *old = get_var_environment(args->env, "OLDPWD");
  if (chdir(old) < 0)
  {
    perror("42sh");
    args->env->previous_return = 2;
  }
  fprintf(stdout, "%s\n", old);
  free(old);
}

bool builtin_cd(struct simple_command_node *cmd,
                struct command_args *args)
{
  args->env->previous_return = 0;
  char *pwd = get_var_environment(args->env, "PWD");
  if (cmd->argc == 1)
  {
    char *home = get_var_environment(args->env, "HOME");
    if (chdir(home) < 0)
      args->env->previous_return = 2;
    free(home);
  }
  else if (strcmp(cmd->argv[1], "-") == 0)
    cd_old(args);
  else
  {
    if (chdir(cmd->argv[1]) < 0)
    {
      perror("42sh");
      args->env->previous_return = 2;
    }
  }
  if (args->env->previous_return == 0)
  {
    modify_var_environment(args->env, "OLDPWD", pwd);
    char *new_pwd = malloc(PATH_MAX);
    getcwd(new_pwd, PATH_MAX);
    modify_var_environment(args->env, "PWD", new_pwd);
    free(new_pwd);
  }
  free(pwd);
  return false;
}
/**
** @file builtin_export.c
** @brief Builtin export
** @authors xu_f
*/

# include <stdio.h>
# include <hash_table.h>
# include <ctype.h>
# include "builtin.h"
# include "env_var.h"

struct export_wrapper
{
  bool no_exported;
  bool print;
};

static void print_export(struct command_args *args)
{
  size_t nb_vars = 0;
  void **env_vars = get_all_value_htable(args->env->env_vars, &nb_vars);
  for (size_t i = 0; i < nb_vars; ++i)
  {
    struct env_var *var = env_vars[i];
    if (var->exported)
      fprintf(stdout, "export %s=\"%s\"\n", var->key, var->value);
  }
  free(env_vars);
}

static inline void print_usage(FILE *file)
{
  fprintf(file,
          "export: usage: export [-n] [name[=value] ...] or export -p\n");
}

static bool export_option(int argc, char **argv, int *argindex,
                          struct export_wrapper *exp)
{
  while (*argindex < argc && argv[*argindex][0] == '-')
  {
    char *option = argv[*argindex] + 1;
    if (strcmp(option, "p") == 0)
      exp->print = true;
    else if (strcmp(option, "n") == 0)
      exp->no_exported = true;
    else
    {
      fprintf(stderr, "42sh: export: -%s: invalid option\n", option);
      print_usage(stderr);
      return false;
    }
    ++*argindex;
  }
  return true;
}

static int check_assignment(char *c)
{
  if (!c || !*c || *c == '=' || isdigit(*c))
    return 0;
  for (; *c; ++c)
  {
    if (*c == ' ')
      return 0;
    if (*c == '=')
      return 2;
  }
  return 1;
}

static void assign_variable(char *var, struct command_args *args,
                            struct export_wrapper *exp)
{
  char *value = NULL;
  var = strtok_r(var, "=", &value);
  set_var_environment(args->env, var, value, !exp->no_exported, false);
}

static bool export_vars(struct simple_command_node *cmd, int arg_index,
                        struct export_wrapper *exp, struct command_args *args)
{
  for (; arg_index < cmd->argc; ++arg_index)
  {
    char *var = cmd->argv[arg_index];
    int check_return = check_assignment(var);
    if (!check_return)
    {
      fprintf(stderr, "42sh: export: '%s': not a valid identifier\n", var);
      return false;
    }
    if (check_return == 1)
    {
      if (!exp->no_exported)
        set_exported(args->env, var);
      else
        remove_exported(args->env, var);
    }
    else
      assign_variable(var, args, exp);
  }
  return true;
}

bool builtin_export(struct simple_command_node *cmd,
                    struct command_args *args)
{
  args->env->previous_return = 0;
  struct export_wrapper options = {0};
  int argindex = 1;
  if (!export_option(cmd->argc, cmd->argv, &argindex, &options))
  {
    args->env->previous_return = 2;
    return false;
  }
  if (argindex == cmd->argc)
    print_export(args);
  else if (!export_vars(cmd, argindex, &options, args))
    args->env->previous_return = 2;
  return false;
}
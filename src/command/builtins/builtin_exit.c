/**
** @file builtin_exit.c
** @brief Builtin exit
** @authors xu_f
*/

# include <stdio.h>
# include "builtin.h"

bool builtin_exit(struct simple_command_node *cmd,
                  struct command_args *args)
{
  int exit_value = 0;
  if (cmd->argc > 1)
  {
    char *save_ptr = cmd->argv[1];
    long int converted_value = strtol(cmd->argv[1], &save_ptr, 10);
    if (*save_ptr)
    {
      exit_value = 2;
      fprintf(stderr, "42sh: exit: %s: numeric argument required\n",
              cmd->argv[1]);
    }
    else
      exit_value = (int)(converted_value % 256);
  }
  args->env->previous_return = exit_value;
  return true;
}
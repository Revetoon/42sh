/**
** @file builtin_env.c
** @brief Builtin env
** @authors xu_f
*/

# include <stdio.h>
# include <zconf.h>
# include "builtin.h"

static void print_environ(void)
{
  char **env = __environ;
  for (; *env; ++env)
    fprintf(stdout, "%s\n", *env);
}

bool builtin_env(struct simple_command_node *cmd,
                 struct command_args *args)
{
  (void)cmd;
  print_environ();
  args->env->previous_return = 0;
  return false;
}
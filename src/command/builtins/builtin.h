/**
** @file builtin.h
** @brief Builtin wrapper header
** @authors xu_f
*/

#ifndef INC_42SH_BUILTIN_H
#define INC_42SH_BUILTIN_H

# include <assert.h>
# include <memory.h>
# include <stdbool.h>
# include <stdlib.h>

# include <ast.h>
# include <command_args.h>

/**
** @brief Builtin exit
** @param cmd ast command node
** @param args command argument structure
** @return true if 42sh must exit
*/
bool builtin_exit(struct simple_command_node *cmd,
                  struct command_args *args);

/**
** @brief Builtin env
** @param cmd ast command node
** @param args command argument structure
** @return true if 42sh must exit
*/
bool builtin_env(struct simple_command_node *cmd,
                 struct command_args *args);

/**
** @brief Builtin cd
** @param cmd ast command node
** @param args command argument structure
** @return true if 42sh must exit
*/
bool builtin_cd(struct simple_command_node *cmd,
                struct command_args *args);

/**
** @brief Builtin export
** @param cmd ast command node
** @param args command argument structure
** @return true if 42sh must exit
*/
bool builtin_export(struct simple_command_node *cmd,
                    struct command_args *args);

#endif /* !INC_42SH_BUILTIN_H */
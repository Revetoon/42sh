/**
** @file command.h
** @brief Command args header
** @authors xu_f
*/

#ifndef INC_42SH_COMMAND_H
#define INC_42SH_COMMAND_H

# include "environment.h"

/**
** @struct command_args
** @brief Command args structure
*/
struct command_args
{
  struct environment *env; /*!< environment */
};

#endif /* !INC_42SH_COMMAND_H */
/**
** @file exec_ast.h
** @brief Ast execution wrapper header
** @authors xu_f
*/

#ifndef INC_42SH_EXEC_AST_H
#define INC_42SH_EXEC_AST_H

# include "ast.h"
# include "command_args.h"

/**
** @brief Execute ast wrapper
** @param ast ast node
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_asts(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute and node
** @param ast ast node of type NODE_AND
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_and(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute or node
** @param ast ast node of type NODE_OR
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_or(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute simple command node
** @param simple_command ast node of type NODE_SIMPLE_COMMAND
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_simple_command(struct ast_node *simple_command,
                         struct command_args *args);

/**
** @brief Execute assignment node
** @param ast ast node of type NODE_ASSIGNMENT
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_assignment(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute pipe node
** @param ast ast node of type NODE_PIPE
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_pipe(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute if node
** @param ast ast node of type NODE_IF
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_if(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute while node
** @param ast ast node of type NODE_WHILE
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_while(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute until node
** @param ast ast node of type NODE_UNTIL
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_until(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute for node
** @param ast ast node of type NODE_FOR
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_for(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute case node
** @param ast ast node of type NODE_CASE
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_case(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute redirection node
** @param ast ast node of type NODE_REDIRECTION
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_redirection(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute function node
** @param ast ast node of type NODE_FUNCTION
** @param args command arguments
** @return true if 42sh must exit
*/
bool exec_function(struct ast_node *ast, struct command_args *args);

/**
** @brief Execute assignment
** @param ast ast node of type NODE_ASSIGNMENT
** @param args command arguments
** @param exported true if exported variable
** @param readonly true if readonly variable
** @return true if 42sh must exit
*/
bool assignment(struct ast_node *ast, struct command_args *args,
                bool exported, bool readonly);

/**
** @brief Execute builtin wrapper
** @param cmd simple command node
** @param args command arguments
** @param return_out store the builtin return value
** @return true if node is a builtin
*/
bool execute_builtin(struct simple_command_node *cmd,
                     struct command_args *args, bool *return_out);

#endif /* !INC_42SH_EXEC_AST_H */
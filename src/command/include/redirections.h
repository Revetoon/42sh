/**
** @file redirections.h
** @brief Redirection wrapper header
** @authors xu_f
*/

#ifndef INC_42SH_REDIRECTIONS_H
#define INC_42SH_REDIRECTIONS_H

# include <stdio.h>
# include <stdlib.h>
# include <errno.h>
# include <string.h>
# include "exec_ast.h"

/**
** @brief Execute input redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_input(struct redirection_node *node,
                    struct command_args *args);

/**
** @brief Execute here_doc redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_here_doc(struct redirection_node *node,
                       struct command_args *args);

/**
** @brief Execute input_output redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_input_output(struct redirection_node *node,
                           struct command_args *args);

/**
** @brief Execute input dup redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_input_dup(struct redirection_node *node,
                        struct command_args *args);

/**
** @brief Execute output pipe redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_output_pipe(struct redirection_node *node,
                          struct command_args *args);

/**
** @brief Execute output redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_output(struct redirection_node *node,
                     struct command_args *args);

/**
** @brief Execute append redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_append(struct redirection_node *node,
                     struct command_args *args);

/**
** @brief Execute output dup redirection
** @param node redirection node
** @param args command arguments
** @return true if redirection went well
*/
bool redirect_output_dup(struct redirection_node *node,
                         struct command_args *args);

/**
** @brief Check dup redirection, word type
** @param node redirection node
** @param args command arguments
** @return 0 on error
** @return 1 if word is a number and has been duplicated
** @return -1 if word is not a number
*/
int check_dup(struct redirection_node *node, struct command_args *args);

#endif /* !INC_42SH_REDIRECTIONS_H */
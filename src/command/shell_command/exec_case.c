/**
** @file exec_case.c
** @brief Case node execution
** @authors xu_f
*/

# include <assert.h>
#include <fnmatch.h>
# include "exec_ast.h"

static bool test_condition(struct case_condition *condition, char *string)
{
  assert(condition);
  for (size_t i = 0; i < condition->nb_conditions; ++i)
  {
    if (fnmatch(condition->conditions[i], string, 0))
      return true;
  }
  return false;
}

bool exec_case(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_CASE);
  struct case_node *node = ast->value.case_value;
  for (struct case_condition *ptr = node->cases; ptr; ptr = ptr->next)
  {
    if (test_condition(ptr, node->variable))
      return exec_asts(ptr->do_group, args);
  }
  return false;
}
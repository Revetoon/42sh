/**
** @file exec_for.c
** @brief For node execution
** @authors xu_f
*/

# include <assert.h>
#include <stdlib.h>
#include <memory.h>
#include <expand.h>
# include "exec_ast.h"

bool exec_for(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_FOR);
  struct for_node *node = ast->value.for_value;
  int argc = (int)node->list_size;
  char **argv = calloc((size_t)argc + 1, sizeof (char*));
  for (int i = 0; i < argc; ++i)
    argv[i] = strdup(node->list[i]);
  string_all_expand(&argc, &argv, args->env);
  bool return_value = false;
  for (int i = 0; i < argc; ++i)
  {
    modify_var_environment(args->env, node->index, argv[i]);
    bool do_group_return = exec_asts(node->do_group, args);
    if (do_group_return)
    {
      return_value = true;
      break;
    }
  }
  for (int i = 0; i < argc; ++i)
    free(argv[i]);
  free(argv);
  return return_value;
}
/**
** @file exec_until.c
** @brief Until node execution
** @authors xu_f
*/

# include <assert.h>
# include "exec_ast.h"

bool exec_until(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_UNTIL);
  struct until_node *node = ast->value.until_value;
  while (1)
  {
    bool condition_return = exec_asts(node->condition, args);
    if (condition_return)
      return true;
    if (!args->env->previous_return)
      break;
    bool do_group_return = exec_asts(node->do_group, args);
    if (do_group_return)
      return true;
  }
  return false;
}
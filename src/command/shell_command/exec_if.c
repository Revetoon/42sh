/**
** @file exec_if.c
** @brief If node execution
** @authors xu_f
*/

#include <assert.h>
# include "exec_ast.h"

bool exec_if(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_IF);
  struct if_node *node = ast->value.if_value;
  bool condition_return = exec_asts(node->conditions, args);
  if (condition_return)
    return condition_return;
  if (!args->env->previous_return)
    return exec_asts(node->then_node, args);
  else
    return exec_asts(node->else_node, args);
}
/**
** @file exec_assignment.c
** @brief Assignment execution
** @authors xu_f
*/

#include <assert.h>
#include <stdlib.h>
# include <string.h>
# include "exec_ast.h"


bool assignment(struct ast_node *ast, struct command_args *args,
                bool exported, bool readonly)
{
  assert(ast);
  assert(ast->type == NODE_ASSIGNMENT);
  struct assignment_node *assignment = ast->value.assignment_value;
  switch (assignment->type)
  {
    case AS_EQUAL:
      set_var_environment(args->env, assignment->var, assignment->value,
                          exported, readonly);
      break;
    case AS_PLUS_EQUAL:
    {
      char *s = get_var_environment(args->env, assignment->var);
      s = realloc(s, strlen(s) + strlen(assignment->value) + 1);
      s = strcat(s, assignment->value);
      set_var_environment(args->env, assignment->var, s, exported, readonly);
      free(s);
    }
  }
  return false;
}

bool exec_assignment(struct ast_node *ast, struct command_args *args)
{
  return assignment(ast, args, false, false);
}
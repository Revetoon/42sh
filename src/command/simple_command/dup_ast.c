/**
** @file dup_ast.c
** @brief Duplicated ast for expansion
** @authors xu_f
*/

#include <memory.h>
#include "dup_ast.h"

static struct ast_node *dup_ast_assignment(struct ast_node *ast)
{
  struct ast_node *node = calloc(1, sizeof (struct ast_node));
  if (!node)
    return NULL;
  node->type = ast->type;
  node->end_type = ast->end_type;
  struct assignment_node *value = calloc(1, sizeof (struct assignment_node));
  if (!value)
  {
    free(node);
    return NULL;
  }
  node->value.assignment_value = value;
  struct assignment_node *ref_value = ast->value.assignment_value;
  value->value = strdup(ref_value->value);
  value->type = ref_value->type;
  value->var = strdup(ref_value->var);
  return node;
}

static char **dup_argv(int argc, char **argv)
{
  if (!argv || !argc)
    return NULL;
  char **dup = calloc((size_t)argc + 1, sizeof(char*));
  for (int i = 0; i < argc; ++i)
  {
    dup[i] = strdup(argv[i]);
  }
  return dup;
}

static struct ast_node *dup_ast_simple_command(struct ast_node *ast)
{
  struct ast_node *node = calloc(1, sizeof (struct ast_node));
  if (!node)
    return NULL;
  node->type = ast->type;
  node->end_type = ast->end_type;
  struct simple_command_node *value =
    calloc(1, sizeof (struct simple_command_node));
  if (!value)
  {
    free(node);
    return NULL;
  }
  node->value.simple_command_value = value;
  struct simple_command_node *ref_cmd = ast->value.simple_command_value;
  value->argc = ref_cmd->argc;
  value->prefix = ref_cmd->prefix;
  value->invert = ref_cmd->invert;
  value->argv = dup_argv(ref_cmd->argc, ref_cmd->argv);
  return node;
}

static void free_dup_ast_assignment(struct ast_node *ast)
{
  if (!ast)
    return;
  struct assignment_node *value = ast->value.assignment_value;
  free(value->value);
  free(value->var);
  free(value);
  free(ast);
}

static void free_dup_ast_simple_command(struct ast_node *ast)
{
  if (!ast)
    return;
  struct simple_command_node *value = ast->value.simple_command_value;
  for (int i = 0; i < value->argc; ++i)
    free(value->argv[i]);
  free(value->argv);
  free(value);
  free(ast);
}

void free_dup_ast(struct ast_node *ast)
{
 if (!ast)
   return;
  switch (ast->type)
  {
    case NODE_SIMPLE_COMMAND:
      free_dup_ast_simple_command(ast);
      break;
    case NODE_ASSIGNMENT:
      free_dup_ast_assignment(ast);
      break;
    default:
      break;
  }
}

struct ast_node *dup_ast_exec(struct ast_node *ast)
{
  if (!ast)
    return NULL;
  switch (ast->type)
  {
    case NODE_ASSIGNMENT:
      return dup_ast_assignment(ast);
    case NODE_SIMPLE_COMMAND:
      return dup_ast_simple_command(ast);
    default:
      return NULL;
  }
}
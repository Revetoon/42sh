/**
** @file simple_command.c
** @brief Simple command execution
** @authors xu_f
*/

# include <assert.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/types.h>
# include <unistd.h>
# include <wait.h>
#include "exec_ast.h"
# include "environment.h"
# include <errno.h>

static bool exec_command(struct simple_command_node *cmd,
                         struct command_args *args)
{
  if (cmd->prefix)
  {
    for (struct ast_node *ptr = cmd->prefix; ptr; ptr = ptr->next)
    {
      assignment(ptr, args, true, false);
    }
  }
  if (execvp(cmd->argv[0], cmd->argv) == -1)
  {
    fprintf(stderr, "42sh: %s ", cmd->argv[0]);
    perror("");
    if (errno == ENOENT)
      args->env->previous_return = 127;
    else
      args->env->previous_return = 126;
    return true;
  }
  return true;
}

static bool wait_command(struct simple_command_node *cmd,
                         struct command_args *args, pid_t id)
{
  int status = 0;
  pid_t return_value = 0;
  do {
    return_value = waitpid(id, &status, WUNTRACED | WCONTINUED);
    if (return_value == -1)
    {
      perror("waitpid");
      return false;
    }
  } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  args->env->previous_return = WEXITSTATUS(status);
  if (cmd->invert)
    args->env->previous_return = args->env->previous_return == 0;
  return false;
}

static bool execute_function(struct simple_command_node *cmd,
                             struct command_args *args, bool *return_out)
{
  struct ast_node *node = get_func_environment(args->env, cmd->argv[0]);
  if (!node)
    return false;
  char **save_argv = args->env->argv;
  int save_argc = args->env->argc;
  args->env->argv = cmd->argv;
  args->env->argc = cmd->argc;
  *return_out = exec_asts(node, args);
  if (cmd->invert)
    args->env->previous_return = args->env->previous_return == 0;
  args->env->argv = save_argv;
  args->env->argc = save_argc;
  return true;
}

bool exec_simple_command(struct ast_node *simple_command,
                         struct command_args *args)
{
  assert(simple_command);
  assert(simple_command->type == NODE_SIMPLE_COMMAND);
  struct simple_command_node *cmd = simple_command->value.simple_command_value;
  if (cmd->argv == NULL)
    return exec_asts(cmd->prefix, args);
  bool check_return = false;
  if (execute_function(cmd, args, &check_return))
    return check_return;
  if (execute_builtin(cmd, args, &check_return))
    return check_return;
  pid_t id = fork();
  if (id == -1)
  {
    fprintf(stderr, "Unable to fork\n");
    return false;
  }
  if (!id)
    return exec_command(cmd, args);
  else
    return wait_command(cmd, args, id);
}
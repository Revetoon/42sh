/**
** @file exec_pipe.c
** @brief Ast pipe execution
** @authors xu_f
*/

#include <assert.h>
# include <unistd.h>
#include <stdio.h>
#include <wait.h>
# include "exec_ast.h"

static bool wait_pipe(int pipe_fd[2], pid_t pids[2])
{
  close(pipe_fd[0]);
  close(pipe_fd[1]);
  int status_read = 0;
  int status_write = 0;
  int catch_read = 0;
  int catch_write = 0;
  do
  {
    if (!catch_read)
    {
      waitpid(pids[0], &status_read, WUNTRACED | WCONTINUED);
      catch_read = WIFEXITED(status_read) || WIFSIGNALED(status_read);
    }
    if (!catch_write)
    {
      waitpid(pids[1], &status_write, WUNTRACED | WCONTINUED);
      catch_write = WIFEXITED(status_write) || WIFSIGNALED(status_write);
    }
  } while (!catch_read || !catch_write);
  return WEXITSTATUS(catch_read) || WEXITSTATUS(catch_write);
}

static bool pipe_read_end(struct pipe_node *node, struct command_args *args,
                          int pipe_fd[2])
{
  close(1);
  dup(pipe_fd[1]);
  close(pipe_fd[0]);
  close(pipe_fd[1]);
  exec_asts(node->left, args);
  return true;
}

static bool pipe_write_end(struct pipe_node *node, struct command_args *args,
                           int pipe_fd[2], pid_t read_fork)
{
  pid_t write_fork = fork();
  if (write_fork == -1)
  {
    perror("fork during pipe failed");
    return false;
  }
  if (write_fork == 0)
  {
    close(0);
    dup(pipe_fd[0]);
    close(pipe_fd[0]);
    close(pipe_fd[1]);
    exec_asts(node->right, args);
    return true;
  }
  else
  {
    pid_t pids[2] = {read_fork, write_fork};
    return wait_pipe(pipe_fd, pids);
  }
}

bool exec_pipe(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_PIPE);
  struct pipe_node *node = ast->value.pipe_value;
  int pipe_fd[2];
  if (pipe(pipe_fd) < 0)
  {
    perror("pipe failed");
    args->env->previous_return = 1;
    return false;
  }
  pid_t read_fork = fork();
  if (read_fork == -1)
  {
    perror("pipe failed");
    args->env->previous_return = 1;
    return false;
  }
  if (read_fork == 0)
    return pipe_read_end(node, args, pipe_fd);
  else
    return pipe_write_end(node, args, pipe_fd, read_fork);
}
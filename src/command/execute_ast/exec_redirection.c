/**
** @file exec_redirection.c
** @brief Ast redirection execution wrapper
** @authors xu_f
*/


# include <assert.h>
# include <unistd.h>
#include <wait.h>
#include <ctype.h>
#include <fcntl.h>
#include <limits.h>

# include "redirections.h"

static bool is_number(char *string)
{
  assert(string && *string);
  for (; *string; ++string)
  {
    if (!isdigit(*string))
      return false;
  }
  return true;
}

int check_dup(struct redirection_node *node, struct command_args *args)
{
  if (*node->right == '-')
    return 1;
  if (is_number(node->right))
  {
    char *end_ptr;
    long int fd = strtol(node->right, &end_ptr, 10);
    if (errno == ERANGE || fd < INT_MIN || fd > INT_MAX)
    {
      args->env->previous_return = -ERANGE;
      return 0;
    }
    int ifd = (int)fd;
    if (fcntl(ifd, F_GETFD) == -1)
    {
      args->env->previous_return = -errno;
      return 0;
    }
    dup(ifd);
    return 1;
  }
  else
    return -1;
}

/**
** @struct redirection wrapper
** @brief Redirection execution wrapper
*/
struct redirection_wrapper
{
  enum redirection_type type; /*!< redirection type */
  bool (*func)(struct redirection_node*, struct command_args*);
  /*!< execute redirection function */
};

static struct redirection_wrapper redirections[] =
{
  {REDIRECTION_TYPE_OUTPUT, redirect_output},
  {REDIRECTION_TYPE_INPUT, redirect_input},
  {REDIRECTION_TYPE_APPEND, redirect_append},
  {REDIRECTION_TYPE_OUTPUT_DUP,redirect_output_dup},
  {REDIRECTION_TYPE_INPUT_DUP, redirect_input_dup},
  {REDIRECTION_TYPE_OUTPUT_PIPE, redirect_output_pipe},
  {REDIRECTION_TYPE_HERE_DOC, redirect_here_doc},
  {REDIRECTION_TYPE_INPUT_OUTPUT, redirect_input_output},
  {REDIRECTION_TYPE_OUTPUT, NULL}
};

static bool wait_child(struct command_args *args, pid_t id)
{
  int status = 0;
  do {
    pid_t w = waitpid(id, &status, WUNTRACED | WCONTINUED);
    if (w == -1)
    {
      perror("42sh");
      return false;
    }
  } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  args->env->previous_return = WEXITSTATUS(status);
  if (args->env->previous_return < 0)
  {
    args->env->previous_return = -(args->env->previous_return + 1);
    return true;
  }
  return false;
}

bool exec_redirection(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_REDIRECTION);
  struct redirection_node *node = ast->value.redirection_value;
  pid_t id = fork();
  if (!id)
  {
    for (struct redirection_wrapper *dir = redirections; dir->func; ++dir)
    {
      if (dir->type == node->type)
      {
        close(node->ionumber);
        if (!dir->func(node, args))
        {
          fprintf(stderr, "42sh: %s: %s\n", node->right,
                  strerror(-args->env->previous_return));
          args->env->previous_return = 1;
          return true;
        }
        bool ret_value = exec_asts(node->child, args);
        if (ret_value)
          args->env->previous_return = -args->env->previous_return - 1;
        return true;
      }
    }
    assert(false && "redirection not found");
    return true;
  }
  else
    return wait_child(args, id);
}
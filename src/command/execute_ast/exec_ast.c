/**
** @file exec_ast.c
** @brief Ast execution wrapper
** @authors xu_f
*/

#include <expand.h>
#include <dup_ast.h>
# include "exec_ast.h"

/**
** @struct ast_wrapper
** @brief Ast wrapper structure
*/
struct ast_wrapper
{
  enum node_type node_type; /*!< node type */
  bool (*func)(struct ast_node*, struct command_args*);
  /*!< execution function */
};

static struct ast_wrapper exec_functions[] =
{
  {NODE_CASE, exec_case},
  {NODE_FOR, exec_for},
  {NODE_UNTIL, exec_until},
  {NODE_WHILE, exec_while},
  {NODE_IF, exec_if},
  {NODE_PIPE, exec_pipe},
  {NODE_ASSIGNMENT, exec_assignment},
  {NODE_SIMPLE_COMMAND, exec_simple_command},
  {NODE_OR, exec_or},
  {NODE_AND, exec_and},
  {NODE_REDIRECTION, exec_redirection},
  {NODE_FUNCTION, exec_function},
  {0, NULL}
};

static bool exec_ast(struct ast_node *ast, struct command_args *args)
{
  for (struct ast_wrapper *wrapper = exec_functions;
    wrapper->func; ++wrapper)
  {
    if (ast->type == wrapper->node_type)
      return wrapper->func(ast, args);
  }
  return false;
}

bool exec_asts(struct ast_node *ast, struct command_args *args)
{
  for (; ast ; ast = ast->next)
  {
    if (ast->type == NODE_ASSIGNMENT || ast->type == NODE_SIMPLE_COMMAND)
    {
      struct ast_node *dup = dup_ast_exec(ast);
      expand_all(dup, args->env);
      bool return_value = exec_ast(dup, args);
      free_dup_ast(dup);
      if (return_value)
        return true;
    }
    else if (exec_ast(ast, args))
      return true;
  }
  return false;
}
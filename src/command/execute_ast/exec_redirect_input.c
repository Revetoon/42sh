/**
** @file exec_redirect_input.c
** @brief Ast input redirection execution
** @authors xu_f
*/

# include "redirections.h"

bool redirect_input(struct redirection_node *node,
                    struct command_args *args)
{
  FILE *file = fopen(node->right, "r");
  if (!file)
  {
    args->env->previous_return = -errno;
    return false;
  }
  return true;
}

bool redirect_here_doc(struct redirection_node *node,
                       struct command_args *args)
{
  FILE *file = fmemopen(node->right, strlen(node->right), "r");
  if (!file)
  {
    args->env->previous_return = -errno;
    return false;
  }
  return true;
}

bool redirect_input_output(struct redirection_node *node,
                           struct command_args *args)
{
  FILE *file = fopen(node->right, "w+");
  if (!file)
  {
    args->env->previous_return = -errno;
    return false;
  }
  return true;
}

bool redirect_input_dup(struct redirection_node *node,
                        struct command_args *args)
{
  int check_value = check_dup(node, args);
  if (!check_value)
    return false;
  if (check_value > 0)
    return true;
  return redirect_input(node, args);
}
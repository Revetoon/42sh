/**
** @file exec_function.c
** @brief Ast function execution
** @authors xu_f
*/

# include <assert.h>
# include "exec_ast.h"

bool exec_function(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_FUNCTION);
  set_func_environment(args->env, ast);
  return false;
}
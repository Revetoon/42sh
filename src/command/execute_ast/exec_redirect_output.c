/**
** @file exec_redirect_output.c
** @brief Ast output redirection execution
** @authors xu_f
*/

# include "redirections.h"

bool redirect_output_pipe(struct redirection_node *node,
                          struct command_args *args)
{
  FILE *file = fopen(node->right, "w");
  if (!file)
  {
    args->env->previous_return = -errno;
    return false;
  }
  return true;
}

bool redirect_output(struct redirection_node *node,
                     struct command_args *args)
{
  return redirect_output_pipe(node, args);
}


bool redirect_append(struct redirection_node *node,
                     struct command_args *args)
{
  FILE *file = fopen(node->right, "a");
  if (!file)
  {
    args->env->previous_return = -errno;
    return false;
  }
  return true;
}

bool redirect_output_dup(struct redirection_node *node,
                         struct command_args *args)
{
  int check_value = check_dup(node, args);
  if (!check_value)
    return false;
  if (check_value > 0)
    return true;
  return redirect_output(node, args);
}
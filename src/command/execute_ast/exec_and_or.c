/**
** @file exec_and_or.c
** @brief And and Or ast execution
** @authors xu_f
*/
# include <assert.h>

# include "exec_ast.h"

bool exec_and(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_AND);
  struct and_node *node = ast->value.and_value;
  if (exec_asts(node->left, args))
    return true;
  if (args->env->previous_return)
    return false;
  return exec_asts(node->right, args);
}

bool exec_or(struct ast_node *ast, struct command_args *args)
{
  assert(ast);
  assert(ast->type == NODE_OR);
  struct or_node *node = ast->value.or_value;
  if (exec_asts(node->left, args))
    return true;
  if (!args->env->previous_return)
    return false;
  return exec_asts(node->right, args);
}
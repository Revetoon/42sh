/**
** @file hash_table_extension.c
** @brief Hash table extension
** @authors xu_f
*/

# include <string.h>
#include <assert.h>
#include <stdio.h>
# include "hash_table.h"

struct htable *copy_htable(struct htable *htable,
                           void *(*data_copy)(void*))
{
  if (!htable)
    return NULL;
  struct htable *copy = create_htable(htable->capacity);
  if (!copy)
    return NULL;
  for (size_t i = 0; i < htable->capacity; ++i)
  {
    for (struct pair *ptr = htable->tab[i];
      ptr; ptr = ptr->next)
    {
      size_t keylen = strlen(ptr->key);
      char *new_key = calloc(1 + keylen, sizeof(char));
      new_key = memcpy(new_key, ptr->key, keylen);
      add_htable(copy, new_key, data_copy(ptr->value));
    }
  }
  return copy;
}

void **get_all_value_htable(struct htable *htable, size_t *size_out)
{
  if (!htable)
    return NULL;
  assert(size_out && "size_out has to be initialized!");
  *size_out = 0;
  void **data = calloc(htable->size, sizeof (void*));
  if (!data)
  {
    fprintf(stderr, "Memory allocation failed\n");
    return NULL;
  }
  size_t index = 0;
  for (size_t i = 0; i < htable->capacity; ++i)
  {
    for (struct pair *ptr = htable->tab[i]; ptr; ptr = ptr->next)
    {
      data[index++] = ptr->value;
    }
  }
  assert(htable->size == index);
  *size_out = htable->size;
  return data;
}
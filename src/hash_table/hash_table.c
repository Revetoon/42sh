/**
** @file hash_table.c
** @brief Hash table
** @authors xu_f
*/

# include <assert.h>
# include <string.h>

# include "hash_table.h"

static uint32_t hash(char *data)
{
  assert(data);
  uint32_t h = 0;
  for (; *data; ++data)
  {
    h += *data;
    h += h * 1024;
    h = h ^ (h / 64);
  }
  h += h * 8;
  h = h ^ (h / 2048);
  h += h * 32768;
  return h;
}

struct htable *create_htable(size_t capacity)
{
  struct htable *table = malloc(sizeof(struct htable));
  if (!table)
    return NULL;
  if (capacity == 0)
    capacity++;
  table->tab = calloc(capacity, sizeof(struct pair *));
  if (!table->tab)
  {
    free(table);
    return NULL;
  }
  table->size = 0;
  table->capacity = capacity;
  return table;
}

struct pair *access_htable(struct htable *htable, char *key)
{
  if (!htable)
    return NULL;
  uint32_t h = hash(key);
  size_t index = h % htable->capacity;
  for (struct pair *pair = htable->tab[index];
       pair; pair = pair->next)
  {
    if (pair->hkey == h && strcmp(key, pair->key) == 0)
      return pair;
  }
  return NULL;
}

static struct pair *pair_create(char *key, void *value)
{
  struct pair *p = malloc(sizeof(struct pair));
  if (!p)
    return NULL;
  p->hkey = hash(key);
  p->key = key;
  p->value = value;
  p->next = NULL;
  return p;
}

static void resize_table(struct htable *htable)
{
  if (!htable)
    return;
  size_t new_capacity = htable->capacity * 2;
  struct pair **tab = calloc(new_capacity, sizeof(struct pair *));
  if (!tab)
    return;
  for (size_t i = 0; i < htable->capacity; ++i)
  {
    struct pair *pair = htable->tab[i];
    while (pair)
    {
      struct pair *tmp = pair->next;
      size_t index = pair->hkey % new_capacity;
      pair->next = tab[index];
      tab[index] = pair;
      pair = tmp;
    }
  }
  free(htable->tab);
  htable->tab = tab;
  htable->capacity = new_capacity;
}

int add_htable(struct htable *htable, char *key, void *value)
{
  assert(key);
  if (access_htable(htable, key) != NULL)
    return 0;
  struct pair *p = pair_create(key, value);
  if (!p)
    return 0;
  size_t index = p->hkey % htable->capacity;
  p->next = htable->tab[index];
  htable->tab[index] = p;
  htable->size += 1;
  if (htable->size / htable->capacity > 0.75f)
    resize_table(htable);
  return 1;
}

void remove_htable(struct htable *htable, char *key)
{
  assert(key);
  if (!htable)
    return;
  uint32_t h = hash(key);
  size_t index = h % htable->capacity;
  struct pair *p = htable->tab[index];
  if (!p)
    return;
  if (p->hkey == h && strcmp(p->key, key) == 0)
  {
    htable->tab[index] = p->next;
    free(p);
    htable->size -= 1;
    if (htable->capacity > 50
        && htable->size / htable->capacity < 0.2f)
      resize_table(htable);
    return;
  }
  for (; p && p->next; p = p->next)
  {
    if (p->next->hkey == h && strcmp(p->next->key, key) == 0)
    {
      struct pair *tmp = p->next;
      p->next = tmp->next;
      free(tmp);
      htable->size -= 1;
      if (htable->capacity > 50
          && htable->size / htable->capacity < 0.2f)
        resize_table(htable);
      return;
    }
  }
}

static void clear_pairs(struct pair *pair)
{
  while (pair)
  {
    struct pair *tmp = pair->next;
    free(pair);
    pair = tmp;
  }
}

void free_htable(struct htable *htable)
{
  if (!htable)
    return;
  for (size_t i = 0; i < htable->capacity; ++i)
    clear_pairs(htable->tab[i]);
  free(htable->tab);
  free(htable);
}